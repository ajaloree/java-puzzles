package com.anoop.barclays.lru;

import org.junit.Assert;
import org.junit.Test;

public class LRUCacheTest {
	
	LRUCache<String,String> underTest;
	
	@Test
	public void testEvictionWithCapacity1() {
		
		underTest = new LRUCacheImpl<>(1);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		
		Assert.assertEquals(1, underTest.size());
		Assert.assertEquals("Battle", underTest.get("B")); // B is retained as it was last element to be added
		Assert.assertNull(underTest.get("A"));             // A is evicted as it's oldest element and capacity is one
	}

	@Test
	public void testEvictionWithCapacity3() {
		
		underTest = new LRUCacheImpl<>(3);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		underTest.put("C", "Code");
		underTest.put("E", "Eagle");

		Assert.assertNull(underTest.get("A")); 
		Assert.assertEquals(3, underTest.size());
	}

	@Test
	public void testToStringWithPutOperationsOnly() {
		
		underTest = new LRUCacheImpl<>(3);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		underTest.put("C", "Code");
		
		Assert.assertEquals("<C,Code>,<B,Battle>,<A,Apple>", underTest.toString());
	}

	@Test
	public void testToStringWithPutOperationsAndOneRetrieval() {
		
		underTest = new LRUCacheImpl<>(3);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		underTest.put("C", "Code");
		underTest.get("A");
		
		Assert.assertEquals("<A,Apple>,<C,Code>,<B,Battle>", underTest.toString());
	}

	@Test
	public void testToStringWithPutOperationsAndRetrievalFollowedByPut() {
		
		underTest = new LRUCacheImpl<>(3);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		underTest.put("C", "Code");
		underTest.get("A");
		underTest.put("E", "Eagle");

		Assert.assertEquals("<E,Eagle>,<A,Apple>,<C,Code>", underTest.toString());
	}

	@Test
	public void testToStringWithPutOperationsAndRetrievalFollowedByTwoPuts() {
		
		underTest = new LRUCacheImpl<>(3);
		
		underTest.put("A", "Apple");
		underTest.put("B", "Battle");
		underTest.put("C", "Code");
		underTest.get("A");
		underTest.put("E", "Eagle");
		underTest.put("C", "Carrot");

		Assert.assertEquals("<C,Carrot>,<E,Eagle>,<A,Apple>", underTest.toString());
	}

}
