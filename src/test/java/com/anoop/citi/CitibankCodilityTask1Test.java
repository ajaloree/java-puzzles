package com.anoop.citi;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.citi.CitibankCodilityTask1;

public class CitibankCodilityTask1Test {
	
	CitibankCodilityTask1 underTest = new CitibankCodilityTask1();
	
	@Test
	public void testSolution1() {
		Assert.assertEquals(4, underTest.solution("We test coders. Give us a try? Come on in!"));
	}

	@Test
	public void testSolution2() {
		Assert.assertEquals(2, underTest.solution("We rock."));
	}

	@Test
	public void testSolution3() {
		Assert.assertEquals(1, underTest.solution("Hello"));
	}

	@Test
	public void testSolution4() {
		Assert.assertEquals(2, underTest.solution("Forget  CVs..Save time . x x"));
	}

	@Test
	public void testSolution5() {
		Assert.assertEquals(1, underTest.solution("Hello!"));
	}

	@Test
	public void testSolution6() {
		Assert.assertEquals(2, underTest.solution("Hello!!!! Anybody there?"));
	}

	@Test
	public void testSolution7() {
		Assert.assertEquals(0, underTest.solution(null));
	}

	@Test
	public void testSolution8() {
		Assert.assertEquals(0, underTest.solution(""));
	}


}
