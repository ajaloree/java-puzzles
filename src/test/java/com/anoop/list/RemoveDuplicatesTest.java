package com.anoop.list;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.anoop.list.ds.ListNode;

public class RemoveDuplicatesTest {
	
	ListNode<String> first = null;
	
	@Before
	public void init() {

		ListNode<String> a = new ListNode<String>("a"); 
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c1 = new ListNode<String>("c");
		ListNode<String> c2 = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		ListNode<String> e = new ListNode<String>("e");
		ListNode<String> f1 = new ListNode<String>("f");
		ListNode<String> f2 = new ListNode<String>("f");
		a.next = b;
		b.next = c1;
		c1.next = c2;
		c2.next = d;
		d.next = e;
		e.next = f1;
		f1.next = f2;
		
		first = a;
	}

	@Test
	public void testRemoveDuplicatesUsingBuffer() {
		
		RemoveDuplicates.removeDuplicatesUsingBuffer(first);

		Assert.assertEquals("a", first.item);
		Assert.assertEquals("b", first.next.item);
		Assert.assertEquals("c", first.next.next.item);
		Assert.assertEquals("d", first.next.next.next.item);
		Assert.assertEquals("e", first.next.next.next.next.item);
		Assert.assertEquals("f", first.next.next.next.next.next.item);
		Assert.assertNull(first.next.next.next.next.next.next);
	}

	@Test
	public void testRemoveDuplicatesWithoutBuffer() {
		
		RemoveDuplicates.removeDuplicatesWithoutBuffer(first);

		Assert.assertEquals("a", first.item);
		Assert.assertEquals("b", first.next.item);
		Assert.assertEquals("c", first.next.next.item);
		Assert.assertEquals("d", first.next.next.next.item);
		Assert.assertEquals("e", first.next.next.next.next.item);
		Assert.assertEquals("f", first.next.next.next.next.next.item);
		Assert.assertNull(first.next.next.next.next.next.next);
	}

}
