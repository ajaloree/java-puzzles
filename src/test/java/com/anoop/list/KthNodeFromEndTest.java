package com.anoop.list;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.list.ds.ListNode;

public class KthNodeFromEndTest {

	@Test
	public void testElementFromEndWithTwoItemsList() {
		
		ListNode<String> a = new ListNode<String>("a"); 
		ListNode<String> b = new ListNode<String>("b");
		a.next = b;
		
		Assert.assertEquals("b", KthNodeFromEnd.elementFromEnd(1, a).item);
		Assert.assertEquals("a", KthNodeFromEnd.elementFromEnd(2, a).item);
		Assert.assertNull(KthNodeFromEnd.elementFromEnd(3, a));
	}

	@Test
	public void testElementFromEndWithFourItemsList() {
		
		ListNode<String> a = new ListNode<String>("a"); 
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		ListNode<String> e = new ListNode<String>("e");
		a.next = b;
		b.next = c;
		c.next = d;
		d.next = e;
		
		Assert.assertEquals("e", KthNodeFromEnd.elementFromEnd(1, a).item);
		Assert.assertEquals("d", KthNodeFromEnd.elementFromEnd(2, a).item);
		Assert.assertEquals("c", KthNodeFromEnd.elementFromEnd(3, a).item);
		Assert.assertEquals("b", KthNodeFromEnd.elementFromEnd(4, a).item);
		Assert.assertEquals("a", KthNodeFromEnd.elementFromEnd(5, a).item);
		Assert.assertNull(KthNodeFromEnd.elementFromEnd(6, a));
	}
}