package com.anoop.list;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.list.ds.ListNode;

public class DeleteMiddleNodeTest {

	@Test
	public void testDeleteNode() {
		
		ListNode<String> a = new ListNode<String>("a"); 
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		ListNode<String> e = new ListNode<String>("e");
		ListNode<String> f = new ListNode<String>("f");

		a.next = b;
		b.next = c;
		c.next = d;
		d.next = e;
		e.next = f;
		
		DeleteMiddleNode.deleteNode(c);
		
		Assert.assertEquals("a", a.item);
		Assert.assertEquals("b", a.next.item);
		Assert.assertEquals("d", a.next.next.item);
		Assert.assertEquals("e", a.next.next.next.item);
		Assert.assertEquals("f", a.next.next.next.next.item);
	}

}