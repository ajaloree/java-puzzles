package com.anoop.list;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.list.ds.ListNode;

public class ReverseListTest {
	
	@Test
	public void testReverseRecursive() {
		ListNode<String> a = new ListNode<String>("a");
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		a.next = b;
		b.next = c;
		c.next = d;
		
		ListNode<String> first = ReverseList.reverseRecursive(a);
		Assert.assertEquals("d", first.item);
		Assert.assertEquals("c", first.next.item);
		Assert.assertEquals("b", first.next.next.item);
		Assert.assertEquals("a", first.next.next.next.item);
	}

	@Test
	public void testReverseIterative() {
		ListNode<String> a = new ListNode<String>("a");
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		a.next = b;
		b.next = c;
		c.next = d;
		
		ListNode<String> first = ReverseList.reverseIterative(a);
		Assert.assertEquals("d", first.item);
		Assert.assertEquals("c", first.next.item);
		Assert.assertEquals("b", first.next.next.item);
		Assert.assertEquals("a", first.next.next.next.item);
	}

}