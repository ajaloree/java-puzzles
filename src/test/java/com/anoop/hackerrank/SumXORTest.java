package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

public class SumXORTest {
	
	@Test
	public void testSumXor() {
		Assert.assertEquals(4, SumXOR.sumXor(4));
		Assert.assertEquals(4, SumXOR.sumXor(10));
		Assert.assertEquals(1073741824, SumXOR.sumXor(1000000000000000l));
	}

}
