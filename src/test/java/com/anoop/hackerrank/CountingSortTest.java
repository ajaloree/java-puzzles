package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.CountingSort;

public class CountingSortTest {
		
	@Test
	public void testCountSort() {
		List<Integer> ret = CountingSort.countingSort(List.of(13, 14, 14, 14, 15, 1, 2, 2));
		Assert.assertEquals(100, ret.size());
		Assert.assertEquals(Integer.valueOf(1), ret.get(1));
		Assert.assertEquals(Integer.valueOf(2), ret.get(2));
		Assert.assertEquals(Integer.valueOf(1), ret.get(13));
		Assert.assertEquals(Integer.valueOf(3), ret.get(14));
		Assert.assertEquals(Integer.valueOf(1), ret.get(15));
	}
}