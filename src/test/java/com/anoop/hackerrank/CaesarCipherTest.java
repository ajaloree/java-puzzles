package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.CaesarCipher;

public class CaesarCipherTest {
	
	@Test
	public void testAllLowerCase() {
		Assert.assertEquals("fghijklmnopqrstuvwxyzabcde", CaesarCipher.caesarCipher("abcdefghijklmnopqrstuvwxyz", 5));
	}

	@Test
	public void testAllUpperCase() {
		Assert.assertEquals("KLMNOPQRSTUVWXYZABCDEFGHIJ", CaesarCipher.caesarCipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 10));
	}

	@Test
	public void testMixedCase() {
		Assert.assertEquals("ddDDeeEE", CaesarCipher.caesarCipher("aaAAbbBB", 3));
	}

	@Test
	public void testMixedCaseWithNonAlphaChars() {
		Assert.assertEquals("dd--DD++ee@@EE**", CaesarCipher.caesarCipher("aa--AA++bb@@BB**", 3));
	}

	@Test
	public void testEncryptDecrypt() {
		Assert.assertEquals("dqrrs", CaesarCipher.caesarCipher("anoop", 3));
		Assert.assertEquals("anoop", CaesarCipher.caesarCipher("dqrrs", -3));
	}

}