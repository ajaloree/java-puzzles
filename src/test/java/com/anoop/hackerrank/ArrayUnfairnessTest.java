package com.anoop.hackerrank;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.ArrayUnfairness;

public class ArrayUnfairnessTest {

	@Test
	public void testMaxMin() {
		Assert.assertEquals(1, ArrayUnfairness.maxMin(Stream.of(1,4,7,2).collect(Collectors.toList()),2));
	}
	
}
