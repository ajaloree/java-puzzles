package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.RecursiveDigitSum;

public class RecursiveDigitSumTest {

	@Test
	public void testSuperDigit() {
		Assert.assertEquals(8, RecursiveDigitSum.superDigit("9875", 4));
		Assert.assertEquals(8, RecursiveDigitSum.superDigit("593", 10));
		Assert.assertEquals(8, RecursiveDigitSum.superDigit("593", 10000*10000));
	}

}