package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.Pangrams;

public class PangramsTest {

	@Test
	public void testPangrams() {
		Assert.assertEquals("not pangram", Pangrams.pangrams("The quick"));
		Assert.assertEquals("pangram", Pangrams.pangrams("The quick brown fox jumps over the lazy dog"));
	}

}