package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.DiagonalDifference;

public class DiagonalDifferenceTest {

	@Test
	public void testFor2by2matix() {
		
    	List<Integer> l1 = List.of(1, 2);
    	List<Integer> l2 = List.of(3, 5);
    	List<List<Integer>> matrix = List.of(l1, l2);
    	
    	Assert.assertEquals(1, DiagonalDifference.diagonalDifference(matrix));
	}

	@Test
	public void testFor3by3matix() {
		
    	List<Integer> l1 = List.of(1, 2, 3);
    	List<Integer> l2 = List.of(4, 5, 6);
    	List<Integer> l3 = List.of(9, 8, 9);
    	List<List<Integer>> matrix = List.of(l1, l2, l3);
    	
    	Assert.assertEquals(2, DiagonalDifference.diagonalDifference(matrix));
	}

}
