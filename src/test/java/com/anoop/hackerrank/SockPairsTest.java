package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.SockPairs;

public class SockPairsTest {

	@Test
	public void testPairsForThreeElements() {
		Assert.assertEquals(0, SockPairs.sockMerchant(List.of(1,2,3)));
		Assert.assertEquals(1, SockPairs.sockMerchant(List.of(1,2,2)));
		Assert.assertEquals(1, SockPairs.sockMerchant(List.of(2,2,2)));
	}

	@Test
	public void testPairsForFourElements() {
		Assert.assertEquals(0, SockPairs.sockMerchant(List.of(1,2,3,4)));
		Assert.assertEquals(1, SockPairs.sockMerchant(List.of(1,2,2,3)));
		Assert.assertEquals(2, SockPairs.sockMerchant(List.of(2,2,2,2)));
	}

}
