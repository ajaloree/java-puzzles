package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.MergeLists.SinglyLinkedListNode;

public class MergeListsTest {

	@Test
	public void testMergeTwoListsWithSingleElementInEach() {
		
		SinglyLinkedListNode _1L1 = new SinglyLinkedListNode(5);
		
		SinglyLinkedListNode _1L2 = new SinglyLinkedListNode(1);
		
		SinglyLinkedListNode merged = MergeLists.mergeLists(_1L1, _1L2);

		Assert.assertEquals("1 -> 5 -> null", MergeLists.printForwardTraversal(merged));
	}

	@Test
	public void testMergeTwoListsWithMoreThanTwoElementsInEach() {
		
		SinglyLinkedListNode _1L1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _3L1 = new SinglyLinkedListNode(3);
		SinglyLinkedListNode _5L1 = new SinglyLinkedListNode(5);
		_1L1.next = _3L1;
		_3L1.next = _5L1;
		
		SinglyLinkedListNode _1L2 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2L2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _4L2 = new SinglyLinkedListNode(4);
		_1L2.next = _2L2;
		_2L2.next = _4L2;
		
		SinglyLinkedListNode merged = MergeLists.mergeLists(_1L1, _1L2);

		Assert.assertEquals("1 -> 1 -> 2 -> 3 -> 4 -> 5 -> null", MergeLists.printForwardTraversal(merged));
	}
	
}
