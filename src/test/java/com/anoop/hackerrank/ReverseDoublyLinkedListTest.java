package com.anoop.hackerrank;

import static com.anoop.hackerrank.ReverseDoublyLinkedList.*;

import org.junit.Assert;
import org.junit.Test;

public class ReverseDoublyLinkedListTest {
	

	@Test
	public void testReverseOneNode() {
		
		DoublyLinkedListNode _1 = new DoublyLinkedListNode(1);
		
		Assert.assertEquals("1 -> null", printForwardTraversal(_1));
		Assert.assertEquals("1 -> null", printBackwardTraversal(_1));
		
		DoublyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("1 -> null", printForwardTraversal(reversed));		
	}

	@Test
	public void testReverseTwoNodes() {
		
		DoublyLinkedListNode _1 = new DoublyLinkedListNode(1);
		DoublyLinkedListNode _2 = new DoublyLinkedListNode(2);
		_1.next = _2;
		_2.prev = _1;
		
		Assert.assertEquals("1 -> 2 -> null", printForwardTraversal(_1));
		Assert.assertEquals("2 -> 1 -> null", printBackwardTraversal(_2));
		
		DoublyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("2 -> 1 -> null", printForwardTraversal(reversed));		
	}

	@Test
	public void testReverseThreeNodes() {
		
		DoublyLinkedListNode _1 = new DoublyLinkedListNode(1);
		DoublyLinkedListNode _2 = new DoublyLinkedListNode(2);
		DoublyLinkedListNode _3 = new DoublyLinkedListNode(3);
		_1.next = _2;
		_2.prev = _1;
		_2.next = _3;
		_3.prev = _2;
		
		Assert.assertEquals("1 -> 2 -> 3 -> null", printForwardTraversal(_1));
		Assert.assertEquals("3 -> 2 -> 1 -> null", printBackwardTraversal(_3));
		
		DoublyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("3 -> 2 -> 1 -> null", printForwardTraversal(reversed));		
	}

}