package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

public class CounterGameTest {

	@Test
	public void testIsPowerOfTwo() {
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 1));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 2));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 3));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 4));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 5));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 6));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 7));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 8));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 9));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 10));
		Assert.assertTrue(CounterGame.isPowerOfTwo(2 << 11));

		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 1)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 2)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 3)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 4)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 5)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 6)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 7)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 8)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 9)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 10)+1));
		Assert.assertFalse(CounterGame.isPowerOfTwo((2 << 11)+1));
	}
	
	@Test
	public void testNearestPowerOfTwo() {
		
		Assert.assertEquals(2, CounterGame.nearestPowerOfTwo((1 << 1)+1));
		Assert.assertEquals(4, CounterGame.nearestPowerOfTwo((1 << 2)+2));
		Assert.assertEquals(8, CounterGame.nearestPowerOfTwo((1 << 3)+3));
		Assert.assertEquals(16, CounterGame.nearestPowerOfTwo((1 << 4)+4));
		Assert.assertEquals(32, CounterGame.nearestPowerOfTwo((1 << 5)+5));
		Assert.assertEquals(64, CounterGame.nearestPowerOfTwo((1 << 6)+6));
		Assert.assertEquals(128, CounterGame.nearestPowerOfTwo((1 << 7)+7));
		Assert.assertEquals(256, CounterGame.nearestPowerOfTwo((1 << 8)+8));
		Assert.assertEquals(512, CounterGame.nearestPowerOfTwo((1 << 9)+9));
		Assert.assertEquals(1024, CounterGame.nearestPowerOfTwo((1 << 10)+10));
		Assert.assertEquals(2048, CounterGame.nearestPowerOfTwo((1 << 11)+11));
		Assert.assertEquals(4096, CounterGame.nearestPowerOfTwo((1 << 12)+12));
		Assert.assertEquals(8192, CounterGame.nearestPowerOfTwo((1 << 13)+13));
		Assert.assertEquals(16384, CounterGame.nearestPowerOfTwo((1 << 14)+14));
		Assert.assertEquals(32768, CounterGame.nearestPowerOfTwo((1 << 15)+15));
		Assert.assertEquals(65536, CounterGame.nearestPowerOfTwo((1 << 16)+16));
		Assert.assertEquals(131072, CounterGame.nearestPowerOfTwo((1 << 17)+17));
		Assert.assertEquals(262144, CounterGame.nearestPowerOfTwo((1 << 18)+18));
		Assert.assertEquals(524288, CounterGame.nearestPowerOfTwo((1 << 19)+19));
		Assert.assertEquals(1048576, CounterGame.nearestPowerOfTwo((1 << 20)+20));
	}
	
	@Test
	public void testCounterGame() {
		Assert.assertEquals("Richard", CounterGame.counterGame(6));		
		Assert.assertEquals("Richard", CounterGame.counterGame(1073741824));
		Assert.assertEquals("Louise", CounterGame.counterGame(1459730561));
		Assert.assertEquals("Louise", CounterGame.counterGame(2048));
	}
}
