package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class IceCreamParlorTest {

	@Test
	public void testIceCreamParlor() {
		
		List<Integer> ls = List.of(Integer.valueOf(1), 
								   Integer.valueOf(3), 
								   Integer.valueOf(5), 
								   Integer.valueOf(7), 
								   Integer.valueOf(11));
		
		List<Integer> ans = IceCreamParlor.iceCreamParlor(10, ls);
		System.out.println(ans);
		Assert.assertEquals(Integer.valueOf(2), ans.get(0));
		Assert.assertEquals(Integer.valueOf(4), ans.get(1));
	}
	
}
