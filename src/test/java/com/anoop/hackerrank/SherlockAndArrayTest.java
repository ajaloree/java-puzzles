package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class SherlockAndArrayTest {
	
	@Test
	public void testBalancedSum() {
		
		Assert.assertEquals("YES", SherlockAndArray.balancedSums(List.of(2,0,0,0)));
		Assert.assertEquals("YES", SherlockAndArray.balancedSums(List.of(5,6,8,11)));
		Assert.assertEquals("NO", SherlockAndArray.balancedSums(List.of(1,2,3)));
		
	}

}
