package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.TimeConversion;

public class TimeConversionTest {

	TimeConversion underTest = new TimeConversion();
	
	@Test
	public void testForMidNight() {
		Assert.assertEquals("00:05:04", underTest.timeConversion("12:05:04AM"));
	}

	@Test
	public void testFor12PMAfternoon() {
		Assert.assertEquals("12:05:04", underTest.timeConversion("12:05:04PM"));
	}

	@Test
	public void testForTimeBefore12PM() {
		Assert.assertEquals("08:05:04", underTest.timeConversion("08:05:04AM"));
	}

	@Test
	public void testForTimeAfter12PM() {
		Assert.assertEquals("20:05:04", underTest.timeConversion("08:05:04PM"));
	}

}
