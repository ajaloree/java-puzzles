package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.Median;

public class MedianTest {
	
	@Test
	public void testMedianForOneElementList() {
		Assert.assertEquals(5, Median.findMedian(List.of(5)));
	}

	@Test
	public void testMedianForThreeElementList() {
		Assert.assertEquals(3, Median.findMedian(List.of(1,5,3)));
	}

	@Test
	public void testMedianForSevenElementList() {
		Assert.assertEquals(2, Median.findMedian(List.of(1,5,3,2,7,1,1)));
	}

}
