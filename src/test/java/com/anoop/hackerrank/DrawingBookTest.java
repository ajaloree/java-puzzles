package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.DrawingBook;

public class DrawingBookTest {

	@Test
	public void testPageFlips() {
		Assert.assertEquals(1, DrawingBook.pageCount(5, 3));
		Assert.assertEquals(2, DrawingBook.pageCount(19, 15));
		Assert.assertEquals(3, DrawingBook.pageCount(19, 7));
	}
	
}
