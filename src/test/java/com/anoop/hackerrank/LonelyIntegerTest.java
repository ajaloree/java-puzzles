package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.LonelyInteger;

public class LonelyIntegerTest {

	@Test
	public void testLonelyIntForSingleElementList() {
		Assert.assertEquals(5, LonelyInteger.lonelyinteger(List.of(5)));
	}

	@Test
	public void testLonelyIntForThreeElementList() {
		Assert.assertEquals(6, LonelyInteger.lonelyinteger(List.of(5,6,5)));
	}

}
