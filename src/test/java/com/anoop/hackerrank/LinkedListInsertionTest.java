package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import static com.anoop.hackerrank.LinkedListInsertion.*;

public class LinkedListInsertionTest {
	
	@Test
	public void testInsertAtHead() {
		
		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _3 = new SinglyLinkedListNode(3);
		_1.next = _2;
		_2.next = _3;
		
		Assert.assertEquals("1 -> 2 -> 3 -> null", printForwardTraversal(_1));
				
		SinglyLinkedListNode head = insertNodeAtPosition(_1, 0, 0);
		
		Assert.assertEquals("0 -> 1 -> 2 -> 3 -> null", printForwardTraversal(head));
	}

	@Test
	public void testInsertAtTail() {
		
		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _3 = new SinglyLinkedListNode(3);
		_1.next = _2;
		_2.next = _3;
		
		Assert.assertEquals("1 -> 2 -> 3 -> null", printForwardTraversal(_1));
				
		SinglyLinkedListNode head = insertNodeAtPosition(_1, 4, 3);
		
		Assert.assertEquals("1 -> 2 -> 3 -> 4 -> null", printForwardTraversal(head));
	}

	@Test
	public void testInsertInMiddle() {
		
		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _3 = new SinglyLinkedListNode(3);
		_1.next = _2;
		_2.next = _3;
		
		Assert.assertEquals("1 -> 2 -> 3 -> null", printForwardTraversal(_1));
				
		SinglyLinkedListNode head = insertNodeAtPosition(_1, 4, 2);
		
		Assert.assertEquals("1 -> 2 -> 4 -> 3 -> null", printForwardTraversal(head));
	}
	
	
}
