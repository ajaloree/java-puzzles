package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.ChocolateSharing;

public class ChocolateSharingTest {

	@Test
	public void testForChololateWithMoreSquaresRequestedThanPresentOnChocoloate() {
		Assert.assertEquals(0, ChocolateSharing.numWaysToShareChocBar(List.of(3), 2, 2));
		Assert.assertEquals(0, ChocolateSharing.numWaysToShareChocBar(List.of(3,4,5), 2, 4));
	}

	@Test
	public void testForChololateWithOneSquare() {
		Assert.assertEquals(1, ChocolateSharing.numWaysToShareChocBar(List.of(2), 2, 1));
		Assert.assertEquals(0, ChocolateSharing.numWaysToShareChocBar(List.of(3), 2, 1));
	}

	@Test
	public void testForChololateWithTwoSquares() {
		Assert.assertEquals(0, ChocolateSharing.numWaysToShareChocBar(List.of(2,4), 7, 2));
		Assert.assertEquals(1, ChocolateSharing.numWaysToShareChocBar(List.of(2,4), 6, 2));
		Assert.assertEquals(1, ChocolateSharing.numWaysToShareChocBar(List.of(2,4), 2, 1));
		
	}

	@Test
	public void testForChololateWithThreeSquares() {
		Assert.assertEquals(2, ChocolateSharing.numWaysToShareChocBar(List.of(2,4,2), 6, 2));
		Assert.assertEquals(1, ChocolateSharing.numWaysToShareChocBar(List.of(2,4,1), 5, 2));
		Assert.assertEquals(1, ChocolateSharing.numWaysToShareChocBar(List.of(2,4,1), 2, 1));
		Assert.assertEquals(0, ChocolateSharing.numWaysToShareChocBar(List.of(2,4,1), 7, 1));
	}

}
