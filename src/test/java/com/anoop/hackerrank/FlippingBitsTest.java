package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.FlippingBits;

public class FlippingBitsTest {
	
	@Test
	public void testBitFlip() {
		Assert.assertEquals(4294967286L, FlippingBits.flippingBits(9));
		Assert.assertEquals(4294967294L, FlippingBits.flippingBits(1));
		
	}

}
