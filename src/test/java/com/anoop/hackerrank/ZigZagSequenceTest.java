package com.anoop.hackerrank;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.ZigZagSequence;

public class ZigZagSequenceTest {

	@Test
	public void testZigZagSequence() {
		Assert.assertTrue(Arrays.equals(new int[] {1,4,5,3,2}, ZigZagSequence.findZigZagSequence(new int[] {2,3,5,1,4}, 5)));
	}
	
}
