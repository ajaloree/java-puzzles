package com.anoop.hackerrank;

import org.junit.Assert;
import org.junit.Test;

public class SherlockAndValidStringTest {
	
	@Test
	public void testIsValid() {
		Assert.assertEquals("YES", SherlockAndValidString.isValid("abc"));
		Assert.assertEquals("YES", SherlockAndValidString.isValid("abca"));
		Assert.assertEquals("YES", SherlockAndValidString.isValid("aaabbbcccd"));
		Assert.assertEquals("NO", SherlockAndValidString.isValid("aaabbbcccdd"));
	}

}