package com.anoop.hackerrank;

import static com.anoop.hackerrank.ReverseSinglyLInkedList.*;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.ReverseSinglyLInkedList.SinglyLinkedListNode;

public class ReverseSinglyLInkedListTest {

	@Test
	public void testReverseListWithOneNode() {

		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		
		Assert.assertEquals("1 -> null", printForwardTraversal(_1));
		
		SinglyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("1 -> null", printForwardTraversal(reversed));
	}

	@Test
	public void testReverseListWithTwoNodes() {

		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		_1.next = _2;
		
		Assert.assertEquals("1 -> 2 -> null", printForwardTraversal(_1));
		
		SinglyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("2 -> 1 -> null", printForwardTraversal(reversed));
	}

	@Test
	public void testReverseListWithThreeNodes() {

		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _3 = new SinglyLinkedListNode(3);

		_1.next = _2;
		_2.next = _3;
		
		Assert.assertEquals("1 -> 2 -> 3 -> null", printForwardTraversal(_1));
		
		SinglyLinkedListNode reversed = reverse(_1);
		
		Assert.assertEquals("3 -> 2 -> 1 -> null", printForwardTraversal(reversed));
	}

}
