package com.anoop.hackerrank;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.GridChallenge;

public class GridChallengeTest {
	
	@Test
	public void testSorted() {
		List<String> grid = Stream.of("eabcd","fghij","olkmn","trpqs","xywuv").collect(Collectors.toList());
		Assert.assertEquals("YES", GridChallenge.gridChallenge(grid));
	}
	
	@Test
	public void tesNotSorted() {
		List<String> grid = Stream.of("mpxz","abcd","wlmf").collect(Collectors.toList());
		Assert.assertEquals("NO", GridChallenge.gridChallenge(grid));
	}

}
