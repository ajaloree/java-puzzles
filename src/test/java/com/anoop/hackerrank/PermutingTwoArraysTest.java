package com.anoop.hackerrank;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.PermutingTwoArrays;

public class PermutingTwoArraysTest {

	@Test
	public void testYesForSize2Array() {
		Assert.assertEquals("YES", PermutingTwoArrays.twoArrays(1, List.of(0,1), List.of(0,2)));
	}
	
	@Test
	public void testNoForSize2Array() {
		Assert.assertEquals("NO", PermutingTwoArrays.twoArrays(3, List.of(1,1), List.of(0,2)));
	}

	@Test
	public void testYesForSize3Array() {
		Assert.assertEquals("YES", PermutingTwoArrays.twoArrays(10, List.of(2,1,3), List.of(7,8,9)));
	}

	@Test
	public void testNoForSize4Array() {
		Assert.assertEquals("NO", PermutingTwoArrays.twoArrays(5, List.of(1,2,2,1), List.of(3,3,3,4)));
	}

}
