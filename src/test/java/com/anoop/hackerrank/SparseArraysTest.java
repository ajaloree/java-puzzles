package com.anoop.hackerrank;

import java.util.List;

import static org.hamcrest.Matchers.*;
import org.junit.Assert;
import org.junit.Test;

import com.anoop.hackerrank.SparseArrays;

public class SparseArraysTest {
	
	@Test
	public void testQueryStrings() {
		
		List<Integer> ret = SparseArrays.matchingStrings(List.of("abc","ab","bc","ca","bca","cab","abc","bc","bc"), List.of("ab","abc","bc","xy"));
		Assert.assertThat(ret, is(equalTo(List.of(1,2,3,0))));
	}
}
