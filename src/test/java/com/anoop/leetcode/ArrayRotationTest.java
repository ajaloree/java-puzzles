package com.anoop.leetcode;

import java.util.Arrays;

import org.junit.Test;

public class ArrayRotationTest {

	@Test
	public void testRotateTwoElementArray() {
		int[] input = new int[]{1,2};
		ArrayRotation.rotate(input, 1);
		Arrays.equals(new int[] {2,1}, input);
	}

	@Test
	public void testRotate() {
		int[] input = new int[]{1,2,3,4,5,6,7,8,9};
		ArrayRotation.rotate(input, 2);
		Arrays.equals(new int[] {8,9,1,2,3,4,5,6,7}, input);
	}

}
