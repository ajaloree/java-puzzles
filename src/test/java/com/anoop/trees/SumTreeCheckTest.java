package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class SumTreeCheckTest {
	
	@Test
	public void testIfSumTreeForSingleLevelTree() {
		IntNode root = new IntNode(5);
		Assert.assertFalse(SumTreeCheck.isSumNode(root));
	}

	@Test
	public void testIfSumTreeForTwoLevelTree() {
		
		IntNode _5 = new IntNode(5);
		IntNode _3 = new IntNode(3);
		IntNode _2 = new IntNode(2);
		
		_5.left = _3;
		_5.right = _2;
		
		Assert.assertTrue(SumTreeCheck.isSumNode(_5));

		IntNode _4 = new IntNode(4);
		
		_5.left = _3;
		_5.right = _4;
		
		Assert.assertFalse(SumTreeCheck.isSumNode(_5));
	}

	@Test
	public void testIfSumTreeForThreeLevelTree() {
		
		IntNode _44 = new IntNode(44);
		IntNode _9 = new IntNode(9);
		IntNode _13 = new IntNode(13);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		
		_44.left = _9;
		_44.right = _13;
		_9.left = _4;
		_9.right = _5;
		_13.left = _6;
		_13.right = _7;
		
		Assert.assertTrue(SumTreeCheck.isSumNode(_44));
		
		IntNode _8 = new IntNode(8);

		_13.left = _6;
		_13.right = _8;

		Assert.assertFalse(SumTreeCheck.isSumNode(_44));
	}
}