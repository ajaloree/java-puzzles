package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.StringNode;

public class SymmetricTreeCheckTest {

	@Test
	public void testSymmetricForLevel0() {
		Assert.assertTrue(SymmetricTreeCheck.isTreeSymmetric(new StringNode("a")));
	}

	@Test
	public void testSymmetricForLevel1() {
		
		StringNode a = new StringNode("a");
		StringNode b = new StringNode("b");
		StringNode c = new StringNode("c");
		
		a.left = b;
		a.right = c;

		Assert.assertTrue(SymmetricTreeCheck.isTreeSymmetric(a));
		
		a.left = b;
		a.right = null;

		Assert.assertFalse(SymmetricTreeCheck.isTreeSymmetric(a));
	}

	@Test
	public void testSymmetricForLevel2() {
		
		StringNode a = new StringNode("a");
		StringNode b = new StringNode("b");
		StringNode c = new StringNode("c");
		StringNode d = new StringNode("d");
		StringNode e = new StringNode("e");
		StringNode f = new StringNode("f");
		StringNode g = new StringNode("g");
		
		a.left = b;
		a.right = c;
		b.left = d;
		b.right = e;
		c.left = f;
		c.right = g;
		
		Assert.assertTrue(SymmetricTreeCheck.isTreeSymmetric(a));
		
		c.right = null;

		Assert.assertFalse(SymmetricTreeCheck.isTreeSymmetric(a));
	}
}