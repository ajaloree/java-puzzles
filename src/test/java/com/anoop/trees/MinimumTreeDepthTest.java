package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class MinimumTreeDepthTest {
	
	@Test
	public void testMinTreeDepth1() {
		IntNode _1 = new IntNode(1);
		Assert.assertEquals(0, MinimumTreeDepth.minimumDepth(_1));
	}

	@Test
	public void testMinTreeDepth2() {
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		
		_1.left = _2;
		_1.right = _3;
		_3.left = _4;
		
		Assert.assertEquals(1, MinimumTreeDepth.minimumDepth(_1));
	}

	@Test
	public void testMinTreeDepth3() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);
		IntNode _9 = new IntNode(9);
		IntNode _10 = new IntNode(10);
		IntNode _11 = new IntNode(11);
		
		_1.left = _2;
		_1.right = _3;
		_2.left = _4;
		_2.right = _5;
		_3.left = _6;
		_3.right = _7;
		_4.right = _8;
		_5.right = _9;
		_7.left = _10;
		_7.right = _11;

		Assert.assertEquals(2, MinimumTreeDepth.minimumDepth(_1));
	}
}
