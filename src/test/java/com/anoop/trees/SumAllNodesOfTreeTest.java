package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class SumAllNodesOfTreeTest {
	
	@Test
	public void testSumForSingleLevelTree() {
		IntNode root = new IntNode(5);
		Assert.assertEquals(5, SumAllNodesOfTree.sumFrom(root));
	}

	@Test
	public void testSumForTwoLevelTree() {
		
		IntNode _5 = new IntNode(5);
		IntNode _3 = new IntNode(3);
		IntNode _6 = new IntNode(6);
		
		_5.left = _3;
		_5.right = _6;
		
		Assert.assertEquals(14, SumAllNodesOfTree.sumFrom(_5));
	}

	@Test
	public void testSumForThreeLevelTree() {
		
		IntNode _5 = new IntNode(5);
		IntNode _3 = new IntNode(3);
		IntNode _6 = new IntNode(6);
		IntNode _1 = new IntNode(1);
		IntNode _4 = new IntNode(4);
		IntNode _8 = new IntNode(8);
		IntNode _7 = new IntNode(7);
		
		_5.left = _3;
		_5.right = _6;
		_3.left = _1;
		_3.right = _4;
		_6.left = _8;
		_6.right = _7;
		
		Assert.assertEquals(34, SumAllNodesOfTree.sumFrom(_5));
	}
}