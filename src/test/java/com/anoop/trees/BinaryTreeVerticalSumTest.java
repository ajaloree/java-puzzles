package com.anoop.trees;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class BinaryTreeVerticalSumTest {

	@Test
	public void testSum1() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);
		
		_1.left = _2;
		_1.right = _3;
		_3.left = _5;
		_3.right = _6;
		_5.left = _7;
		_5.right = _8;
		
		Map<Integer, Integer> vertSums = BinaryTreeVerticalSum.getVerticalSum(_1);
		Assert.assertEquals(4, vertSums.size());
		Assert.assertEquals(Integer.valueOf(9), vertSums.get(-1));
		Assert.assertEquals(Integer.valueOf(6), vertSums.get(0));
		Assert.assertEquals(Integer.valueOf(11), vertSums.get(1));
		Assert.assertEquals(Integer.valueOf(6), vertSums.get(2));
	}

	@Test
	public void testSum2() {
		
		IntNode _5 = new IntNode(5);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);
		IntNode _9 = new IntNode(9);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _6 = new IntNode(6);
		IntNode _10 = new IntNode(10);
		
		
		_5.left = _7;
		_5.right = _8;
		_7.left = _9;
		_8.right = _2;
		_9.left = _3;
		_9.right = _4;
		_2.left = _6;
		_2.right = _10;
		
		Map<Integer, Integer> vertSums = BinaryTreeVerticalSum.getVerticalSum(_5);
		Assert.assertEquals(7, vertSums.size());
		Assert.assertEquals(Integer.valueOf(3), vertSums.get(-3));
		Assert.assertEquals(Integer.valueOf(9), vertSums.get(-2));
		Assert.assertEquals(Integer.valueOf(11), vertSums.get(-1));
		Assert.assertEquals(Integer.valueOf(5), vertSums.get(0));
		Assert.assertEquals(Integer.valueOf(14), vertSums.get(1));
		Assert.assertEquals(Integer.valueOf(2), vertSums.get(2));
		Assert.assertEquals(Integer.valueOf(10), vertSums.get(3));
	}
}