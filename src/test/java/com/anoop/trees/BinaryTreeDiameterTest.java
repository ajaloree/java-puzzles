package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.StringNode;

public class BinaryTreeDiameterTest {
	
	@Test
	public void testDiameterThroughRootNode() {
		
		StringNode _1 = new StringNode("1");
		StringNode _2 = new StringNode("2");
		StringNode _3 = new StringNode("3");
		StringNode _4 = new StringNode("4");
		StringNode _5 = new StringNode("5");
		StringNode _6 = new StringNode("6");
		StringNode _7 = new StringNode("7");
		StringNode _8 = new StringNode("8");

		_1.left = _2;
		_1.right = _3;
		_2.right = _4;
		_3.left = _5;
		_3.right = _6;
		_5.left = _7;
		_5.right = _8;
		
		Assert.assertEquals(6, BinaryTreeDiameter.calculateDiameter(_1));
	}

	@Test
	public void testDiameterNotThroughRootNode() {
		
		StringNode _1 = new StringNode("1");
		StringNode _2 = new StringNode("2");
		StringNode _3 = new StringNode("3");
		StringNode _4 = new StringNode("4");
		StringNode _5 = new StringNode("5");
		StringNode _6 = new StringNode("6");
		StringNode _7 = new StringNode("7");

		_1.right = _2;
		_2.left = _3;
		_2.right = _4;
		_3.left = _5;
		_3.right = _6;
		_4.right = _7;
		
		Assert.assertEquals(5, BinaryTreeDiameter.calculateDiameter(_1));
	}
}