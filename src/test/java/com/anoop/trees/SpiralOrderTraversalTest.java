package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class SpiralOrderTraversalTest {
	
	@Test
	public void testSpiralPrint() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);
		IntNode _9 = new IntNode(9);
		IntNode _10 = new IntNode(10);
		IntNode _11 = new IntNode(11);
		IntNode _12 = new IntNode(12);
		IntNode _13 = new IntNode(13);
		IntNode _14 = new IntNode(14);
		IntNode _15 = new IntNode(15);
		
		_15.left = _13;
		_15.right = _14;
		_13.left = _9;
		_13.right = _10;
		_14.left = _11;
		_14.right = _12;
		_9.left = _1;
		_9.right = _2;
		_10.left = _3;
		_10.right = _4;
		_11.left = _5;
		_11.right = _6;
		_12.left = _7;
		_12.right = _8;
				
		Assert.assertEquals("15 14 13 9 10 11 12 8 7 6 5 4 3 2 1", SpiralOrderTraversal.spiralPrintNodes(_15));
	}

}