package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class BinaryTreeMirrorTest {
	
	@Test
	public void testMirrorForBalancedTree() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);

		_1.left = _2;
		_1.right = _3;
		_2.left = _4;
		_2.right = _5;
		_3.left = _6;
		_3.right = _7;
		
		BinaryTreeMirror.mirror(_1);
		
		Assert.assertEquals(Integer.valueOf(3), _1.left.data);
		Assert.assertEquals(Integer.valueOf(2), _1.right.data);
		Assert.assertEquals(Integer.valueOf(7), _3.left.data);
		Assert.assertEquals(Integer.valueOf(6), _3.right.data);
		Assert.assertEquals(Integer.valueOf(5), _2.left.data);
		Assert.assertEquals(Integer.valueOf(4), _2.right.data);
	}

	@Test
	public void testMirrorForUnbalancedTree() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);

		_1.left = _2;
		_1.right = _3;
		_2.right = _5;
		_3.left = _6;
		
		BinaryTreeMirror.mirror(_1);
		
		Assert.assertEquals(Integer.valueOf(3), _1.left.data);
		Assert.assertEquals(Integer.valueOf(2), _1.right.data);
		Assert.assertNull(_3.left);
		Assert.assertEquals(Integer.valueOf(6), _3.right.data);
		Assert.assertEquals(Integer.valueOf(5), _2.left.data);
		Assert.assertNull(_2.right);
	}

}
