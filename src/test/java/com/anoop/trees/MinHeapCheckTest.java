package com.anoop.trees;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class MinHeapCheckTest {
	
	@Test
	public void testTreeIsMinHeap() {
		
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);
		
		_2.left = _3;
		_2.right = _4;
		_2.left = _5;
		_2.right = _6;
		_3.left = _7;
		_3.right = _8;

		Assert.assertTrue(MinHeapCheck.isMinHeapTree(_2));
	}

	@Test
	public void testTreeIsNotMinHeap() {
		
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _8 = new IntNode(8);
		IntNode _10 = new IntNode(10);
		
		_5.left = _3;
		_5.right = _8;
		_3.left = _2;
		_3.right = _4;
		_8.left = _6;
		_8.right = _10;

		Assert.assertFalse(MinHeapCheck.isMinHeapTree(_5));
	}

}
