package com.anoop.trees;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.StringNode;

public class BinaryTreePathsTest {
	
	BinaryTreePaths underTest;
		
	@Test
	public void testPaths() {
		
		StringNode a = new StringNode("A");
		StringNode b = new StringNode("B");
		StringNode c = new StringNode("C");
		StringNode d = new StringNode("D");
		StringNode e = new StringNode("E");
		StringNode f = new StringNode("F");
		StringNode g = new StringNode("G");
		StringNode h = new StringNode("H");
		StringNode i = new StringNode("I");

		a.left = b;
		a.right = c;
		b.left = d;
		b.right = e;
		c.left = f;
		c.right = g;
		g.left = h;
		g.right = i;
		
		underTest = new BinaryTreePaths(a);
		List<String> paths = underTest.getPaths();
		Assert.assertThat(paths, Matchers.hasItems("A->B->D", "A->B->E", "A->C->F", "A->C->G->H", "A->C->G->I"));
		
	}

}
