package com.anoop.trees.views;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;

public class LeftViewBinaryTreeTest {
	
	@Test
	public void testLeftView() {
		
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _4 = new IntNode(4);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);

		_1.left = _2;
		_1.right = _3;
		_2.right = _4;
		_3.left = _5;
		_3.right = _6;
		_5.left = _7;
		_5.right = _8;
		
		List<IntNode> leftView = LeftViewBinaryTree.getLeftTree(_1);
		Assert.assertEquals(4, leftView.size());
		Assert.assertEquals(Integer.valueOf(1), leftView.get(0).data);
		Assert.assertEquals(Integer.valueOf(2), leftView.get(1).data);
		Assert.assertEquals(Integer.valueOf(4), leftView.get(2).data);
		Assert.assertEquals(Integer.valueOf(7), leftView.get(3).data);
	}

}