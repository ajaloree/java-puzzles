package com.anoop.trees.views;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.trees.ds.IntNode;
import com.anoop.trees.views.TopViewBinaryTree;

public class TopViewBinaryTreeTest {
	
	@Test
	public void testTopView() {
	
		IntNode _1 = new IntNode(1);
		IntNode _2 = new IntNode(2);
		IntNode _3 = new IntNode(3);
		IntNode _5 = new IntNode(5);
		IntNode _6 = new IntNode(6);
		IntNode _7 = new IntNode(7);
		IntNode _8 = new IntNode(8);

		_1.left = _2;
		_1.right = _3;
		_3.left = _5;
		_3.right = _6;
		_5.left = _7;
		_5.right = _8;

		Map<Integer, Integer> topView = TopViewBinaryTree.getTopView(_1);
		Assert.assertEquals(4, topView.size());
		Assert.assertEquals(Integer.valueOf(2), topView.get(-1));
		Assert.assertEquals(Integer.valueOf(1), topView.get(0));
		Assert.assertEquals(Integer.valueOf(3), topView.get(1));
		Assert.assertEquals(Integer.valueOf(6), topView.get(2));
	}

}
