package com.anoop.hackerrank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Given an array of integers, where all elements but one occur twice, find the unique element.
 * 
 * @author ajaloree
 */
public class LonelyInteger {

	public static int lonelyinteger(List<Integer> a) {

		AtomicReference<Integer> ret = new AtomicReference<>();
		Map<Integer, Integer> counts = new HashMap<>();
		a.forEach(i -> counts.put(i, counts.getOrDefault(i, 0)+1));
		counts.forEach((k,v) -> {
			if(v == 1) {
				ret.set(k);
			}
		});
		
		return ret.get();
	}
}
