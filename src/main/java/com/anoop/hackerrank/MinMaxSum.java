package com.anoop.hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Given five positive integers, find the minimum and maximum values that can be calculated 
 * by summing exactly four of the five integers. Then print the respective minimum and maximum 
 * values as a single line of two space-separated long integers.
 * 
 * @author ajaloree
 *
 */
public class MinMaxSum {
	
	public static void miniMaxSum(List<Integer> arr) {
		
		List<Integer> sortedArr = arr.stream().sorted().collect(Collectors.toList());
		long sumFirst4 = sortedArr.stream().limit(4).mapToLong(v -> v).sum();
		long sumLast4 = sortedArr.stream().skip(1).mapToLong(v -> v).sum();
		System.out.println(sumFirst4+" "+sumLast4);
	}
	
	public static void main(String[] args) throws Exception {
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
        						  .map(Integer::parseInt)
        						  .collect(Collectors.toList());

        miniMaxSum(arr);

        bufferedReader.close();
	}
}