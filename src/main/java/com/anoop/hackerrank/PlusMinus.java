package com.anoop.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Given an array of integers, calculate the ratios of its elements that are positive, negative, and zero. 
 * Print the decimal value of each fraction on a new line with 6 places after the decimal.
 * 
 * @author ajaloree
 *
 */
public class PlusMinus {

	public static void plusMinus(List<Integer> arr) {
				
		DecimalFormat df = new DecimalFormat("#.000000");
		
		long totPositive = arr.stream().filter(v -> v > 0).count();
		long totNegative = arr.stream().filter(v -> v < 0).count();
		long totZero = arr.stream().filter(v -> v == 0).count();
		
		System.out.println(df.format((double)totPositive/arr.size()));
		System.out.println(df.format((double)totNegative/arr.size()));
		System.out.println(df.format((double)totZero/arr.size()));
	}
	
	
    public static void main(String[] args) throws IOException {
        
    	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
        					.map(Integer::parseInt)
        					.collect(Collectors.toList());

        plusMinus(arr);

        bufferedReader.close();
    }
	
}
