package com.anoop.hackerrank;

import java.util.List;
/**
 * Two children, Lily and Ron, want to share a chocolate bar. 
 * Each of the squares has an integer on it.
 * Lily decides to share a contiguous segment of the bar selected such that:
 * 1. The length of the segment matches Ron's birth month (m), and,
 * 2. The sum of the integers on the squares is equal to his birth day (d).
 * Determine how many ways she can divide the chocolate.
 * 
 * @author ajaloree
 */
public class ChocolateSharing {
	
	/**
	 * 	s	|2	|2	|3	|1	|4	|0	|1	|2	|2	|3  |
	 * ----------------------------------------------
	 * idx	|0	|1	|2	|3	|4	|5	|6	|7	|8	|9  |
	 * 
	 * @param s: the numbers on each of the squares of chocolate
	 * @param d: Ron's birth day
	 * @param m: Ron's birth month
	 * @return the number of ways the bar can be divided
	 */
	public static int numWaysToShareChocBar(List<Integer> s, int d, int m) {
		
		if(m > s.size() || m == 0) {
			return 0;
		} else if(m == s.size()) {
			return (s.stream().reduce((x,y)->x+y).get()==d) ? 1 : 0;
		} else {
			int count = 0;
			for(int i=0; i<=(s.size()-m); i++) {			
				int sum = 0;
				for(int j = 0; j < m; j++) {
					sum+=s.get(i+j);
				}
				if(sum == d) {
					System.out.println("Match found!");
					count++;
				}
			}
			return count;
		}
	}
	
	/**
	 * Function to print elements of array n at a time.
	 * 
	 * @param arr 	Integer array with n elements
	 * @param m 	Number of elements in each group being printed
	 */
	public static void printArray(int[] arr, int m) {
		
		int n = arr.length;
		for(int i = 0; i <= n-m; i++) {
			for(int j = 0; j < m; j++) {
				System.out.print(arr[i+j]);
			}
			System.out.println();
		}
	}
	
	
	public static void main(String[] args) {
		
		printArray(new int[] {1,2}, 1);
		printArray(new int[] {1,2}, 2);

		printArray(new int[] {1,2,3}, 1);
		printArray(new int[] {1,2,3}, 2);

		printArray(new int[] {1,2,3,4}, 2);
		printArray(new int[] {1,2,3,4}, 1);
	}
}