package com.anoop.hackerrank;

/**
 * Given the pointer to the head node of a linked list and an integer 
 * to insert at a certain position, create a new node with the given 
 * integer as its 'data' attribute, insert this node at the desired 
 * position and return the head node.
 * A position of 0 indicates head, a position of 1 indicates one node 
 * away from the head and so on. The head pointer given may be null 
 * meaning that the initial list is empty.
 * Example
 * 'head' refers to the first node in the list 1 -> 2 -> 3
 * Insert a node at position 2 with data = 4. The new list is
 * 1 -> 2 -> 4 -> 3 
 * @author ajaloree
 *
 */
public class LinkedListInsertion {
	
	public static class SinglyLinkedListNode {
		
	    public int data;
	    public SinglyLinkedListNode next;

	    public SinglyLinkedListNode(int nodeData) {
	        this.data = nodeData;
	        this.next = null;
	    }
	}
	
	public static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode llist, int data, int position) {
		
		if(llist == null) {
			
			return new SinglyLinkedListNode(data);
			
		} else if(position == 0) {
			
			SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
			newNode.next = llist;
			return newNode;
			
		} else {
			
			SinglyLinkedListNode current = llist;
			
			int idx = 0;
			while(idx < position-1) {
				
				
				current = current.next;
				idx++;
			}
			
			SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
			newNode.next = current.next;
			current.next = newNode;
					
		    return llist;
		}
	}
	
    public static String printForwardTraversal(SinglyLinkedListNode head) {
    	
    	StringBuilder sb = new StringBuilder();
    	SinglyLinkedListNode current = head;
    	while(current != null) {
    		sb.append(current.data+" -> ");
    		current = current.next;
    	}
    	sb.append("null");
    	return sb.toString();
    }

	public static void main(String[] args) {
	
		SinglyLinkedListNode _1 = new SinglyLinkedListNode(1);
		SinglyLinkedListNode _2 = new SinglyLinkedListNode(2);
		SinglyLinkedListNode _3 = new SinglyLinkedListNode(3);
		_1.next = _2;
		_2.next = _3;
		
		System.out.println(printForwardTraversal(_1));
		
		insertNodeAtPosition(_1, 4, 2);
		
		System.out.println(printForwardTraversal(_1));
		
		
		
		
	}
	
	
}
