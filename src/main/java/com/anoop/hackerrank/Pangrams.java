package com.anoop.hackerrank;

/**
 * A pangram is a string that contains every letter of the alphabet. 
 * Given a sentence determine whether it is a pangram in the English alphabet. 
 * Ignore case. Return either pangram or not pangram as appropriate.
 * Example:
 * s = "The quick brown fox jumped over the lazy dog"
 * The string contains all letters in the English alphabet, so return pangram.
 * @author ajaloree
 *
 */
public class Pangrams {

	public static String pangrams(String s) {
		
		String ret = "pangram";
		s = s.toLowerCase();
		int[] alphArr = new int[26]; // array to keep track of occurrences of each alphabet within string
		int IDX_OFFSET = (int)'a'; // ASCII value of lowercase 'a'
		for(char c: s.toCharArray()) {
			if((int)c < IDX_OFFSET || (int)c > IDX_OFFSET+25) { //only consider aplhabets between a - z. Ignore others
				continue;
			}
			alphArr[(int)c-IDX_OFFSET]++;
		}
		// If any slot in the array has value 0, it menas the character for that slot idx after 'a' wasnt found.
		for(int i : alphArr) {
			if(i == 0) {
				ret = "not "+ret;
				break;
			}
		}
		return ret;
	}
}