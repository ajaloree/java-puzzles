package com.anoop.hackerrank;

import java.util.List;
import java.util.stream.Collectors;

public class Median {

    public static int findMedian(List<Integer> arr) {
        if(arr.isEmpty()) {
            return 0;
        }else if(arr.size() == 1) {
            return arr.get(0);
        } else {
            arr = arr.stream().sorted().collect(Collectors.toList());
            return arr.get(arr.size()/2);    
        }
    }
    
    public static void main(String[] args) {
		
    	System.out.println(List.of(1,5,2,9,3,11).stream().sorted().collect(Collectors.toList()));
    	
	}
    
}
