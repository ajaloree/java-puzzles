package com.anoop.hackerrank;

public class CaesarCipher {

    public static String caesarCipher(String s, int k) {
    	
    	if(s.isBlank()) return s;
    	
    	StringBuilder ret = new StringBuilder(s.length());
    	for(char c : s.toCharArray()) {
    		ret.append(shift(c,k));
    	}
    	return ret.toString();
    }
    
    private static char shift(char c, int k) {
    	if(c >= 'a' && c <= 'z') {
    		return shift(c, 'a', 'z', k);
    	} else if(c >= 'A' && c <= 'Z') {
    		return shift(c, 'A', 'Z', k);
    	} else {
    		return c;
    	}
    }

    private static char shift(char c, char lowerLim, char upperLim, int k) {
		int v = ((int)c + k);
		if(v > upperLim) {
			v = v - upperLim;
			v = lowerLim + (v -1)%26;
		}
		return (char)v;
    }

    public static void main(String[] args) {
		
    	//String input = "There's-a-starman-waiting-in-the-sky";
    	String input = "abcdefghijklmnopqrstuvwxyz";
    	System.out.println("Input: "+input);
    	System.out.println("Output: "+caesarCipher(input, 3));
    	
	}
}
