package com.anoop.hackerrank;

import java.util.List;

/**
 * Given a square matrix, calculate the absolute difference between the sums of its diagonals.
 * For example, the square matrix  is shown below:
 * 1 2 3
 * 4 5 6
 * 7 8 9  
 * The left-to-right diagonal = 1+5+9 = 15. 
 * The right to left diagonal = 3+5+9 = 17. 
 * Their absolute difference is |15-17| = 2.
 * @author ajaloree
 *
 */
public class DiagonalDifference {
	
	/**
	 * 				0	1	2	3
	 * --------|-------------------
	 * List@ 0 | 	1   2   3   4
	 * List@ 1 | 	5   6   7	8
	 * List@ 2 | 	9   10  11  12
	 * List@ 3 |    13  14  15  16
	 * 
	 * @param arr
	 * @return
	 */
    public static int diagonalDifference(List<List<Integer>> arr) {
    	
    	//l2r_sum = arr[0][0]+arr[1][1]+arr[2][2]+arr[3][3]
    	//r2l_sum = arr[0][3]+arr[1][2]+arr[2][1]+arr[3][0]
    	
    	int N = arr.size();
    	int l2r_diagonal_sum = 0;
    	int r2l_diagonal_sum = 0;
    	
    	for(int i = 0; i < N; i++) {
    		l2r_diagonal_sum += arr.get(i).get(i);
    		r2l_diagonal_sum += arr.get(i).get(N-(i+1));
    	}
    	
    	return Math.abs(l2r_diagonal_sum - r2l_diagonal_sum);
    }

}
