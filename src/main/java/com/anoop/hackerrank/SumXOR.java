package com.anoop.hackerrank;

import java.util.stream.Stream;

/**
 * Given an integer 'n', find each 'x' such that:
 * 0 =< x =< n
 * n + x = n XOR x
 * where XOR denotes the bitwise XOR operator. 
 * Return the number of x's satisfying the criteria.
 * 
 * Example: n = 4
 * 
 * There are four values that meet the criteria:
 * 4 + 0 = 4 XOR 0 = 4
 * 4 + 1 = 4 XOR 1 = 5
 * 4 + 2 = 4 XOR 2 = 6
 * 4 + 3 = 4 XOR 3 = 7
 * 
 * Return 4.
 * 
 * Constraint: 
 * 'n' can be very large: n =< 10 to the power of 15. 
 * 
 * @author ajaloree
 *
 */
public class SumXOR {
	
	/**
	 * TODO - Understand below solution. 
	 */
    public static long sumXor(long n) {
        long count = 0;
        while (n > 0) {
            if ((n & 1) == 0) count++;
            n = n>>1;
        }
        return 1L << count;
    }
    
	public static long sumXorInefficient(long n) {
	   
		return Stream.iterate(0, v->v+1).limit(n).parallel().filter(i -> ((n+i) == (n^i))).count();
	}
	
	public static void main(String[] args) {
		
		System.out.println(sumXor(100000000l));
		//System.out.println(1000000000000000l/10000000000l);
		
		
		
		
	}

}
