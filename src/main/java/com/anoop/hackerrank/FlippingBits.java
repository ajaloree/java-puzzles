package com.anoop.hackerrank;

import java.util.stream.Stream;

/**
 * You will be given a list of 32 bit unsigned integers. 
 * Flip all the bits (1->0 and 0->1) and return the result as an unsigned integer.
 * 
 * @author ajaloree
 *
 */
public class FlippingBits {

	public static long flippingBits(long n) {
		
		int SIZE = 32;
		long[] bits = new long[SIZE];
		Integer[] flippedBits = new Integer[SIZE];
		
		for(int i = 31; i >= 0; i--) {
			
			bits[i] = (n & 1);
			flippedBits[i] = (bits[i]==1?0:1);
			n = n >> 1;
		}
		
		String flippedBitsString = Stream.of(flippedBits).map(v -> Long.toString(v)).reduce((x,y)->x+y).get();
		
		return Long.parseLong(flippedBitsString,2);
	}
	
	
	public static String toBinaryString(long n) {

		Long[] bits = new Long[32];
		for(int i = 31; i >= 0; i--) {
			bits[i] = (n & 1);			//Get rightmost bit - least significant digit
			n = n >> 1;					//Push the bits towards right
		}
		return Stream.of(bits).map(v -> v.toString()).reduce((x, y) -> x+y).get();
	}
}
