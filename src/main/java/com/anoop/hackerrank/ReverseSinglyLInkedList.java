package com.anoop.hackerrank;

/**
 * Given the pointer to the head node of a linked list, 
 * change the next pointers of the nodes so that their order is reversed. 
 * The head pointer given may be null meaning that the initial list is empty.
 * Example: 
 * 'head' references the list 1 -> 2 -> 3 -> null 
 * Manipulate the 'next' pointers of each node in place and return 
 * 'head', now referencing the head of the list 3 -> 2 -> 1 -> null.
 * 
 * @author ajaloree
 *
 */
public class ReverseSinglyLInkedList {

	public static class SinglyLinkedListNode {
	     int data;
	     SinglyLinkedListNode next;
	     public SinglyLinkedListNode(int data) {
	    	 this.data = data;
	     }
	}
	
	public static SinglyLinkedListNode reverse(SinglyLinkedListNode llist) {
	
		SinglyLinkedListNode first = llist;
		SinglyLinkedListNode reversed = null;
		while(first != null) {
			SinglyLinkedListNode second = first.next;
			first.next = reversed;
			reversed = first;
			first = second;
		}
		return reversed;
		
	}
	
    public static String printForwardTraversal(SinglyLinkedListNode head) {
    	
    	StringBuilder sb = new StringBuilder();
    	SinglyLinkedListNode current = head;
    	while(current != null) {
    		sb.append(current.data+" -> ");
    		current = current.next;
    	}
    	sb.append("null");
    	return sb.toString();
    }

}