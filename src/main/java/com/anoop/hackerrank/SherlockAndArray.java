package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.List;

/**
 * Watson gives Sherlock an array of integers. 
 * His challenge is to find an element of the array such that the sum of 
 * all elements to the left is equal to the sum of all elements to the right.
 * Example:
 * arr = [5,6,8,11]
 * 8 is between two subarrays that sum to 11.
 * arr = [1]
 * The answer is 1 since left and right sum to 0.
 * You will be given arrays of integers and must determine whether there is an 
 * element that meets the criterion. If there is, return YES. Otherwise, return NO.
 * 
 * @author ajaloree
 *
 */
public class SherlockAndArray {
	
	/**
	 * This function avoids double iteration and keeps updating the left sum on 
	 * every iteration by adding the hinge value to it cumulatively. Hence double
	 * iteration is not required making algorithm performance O(N).
	 * 
	 * @param arr
	 * @return
	 */
    public static String balancedSums(List<Integer> arr) {

    	if(arr.size() == 1) return "YES";
    	
    	int sum = arr.stream().mapToInt(v -> v).sum();
    	
		int sumFromLeft = 0;			
    	
		for(int i = 0; i < arr.size()-1; i++) {
			
			int hinge = arr.get(i);
			int sumFromRight = sum - (sumFromLeft+hinge);
			
			if(sumFromLeft == sumFromRight) {
				System.out.println("Hinge detected at "+hinge);
				System.out.print(" "+hinge+" ");
				System.out.println("Left sum: "+sumFromLeft+" Right sum: "+sumFromRight);
				return "YES";
			}
			
			sumFromLeft += hinge;
		}
		return "NO";
    }

    /**
     * Inefficient algorithm that is very slow for large input - when array size is 10,0000.
     * This is because there is double iteration - the outer loop that moves the hinge one
     * element at a time, and then the inner loop that calculates the sum of left and right
     * sections of the array with hinge as the reference. Even though the right sum is not 
     * computed element by element and is derived based on total sum, left sum and the hinge
     * the fact that there is a double iteration that involves computing left sum right from
     * first element on every iteration makes it slow O(N*N) and inefficient for large problem size.
     * 
     * This weakness is addressed in above function.
     * 
     * @param arr
     * @return
     */
    public static String balancedSumsSlow(List<Integer> arr) {

    	if(arr.size() == 1) return "YES";
    	
    	int sum = arr.stream().mapToInt(v -> v).sum();
    	
		for(int i = 0; i < arr.size()-1; i++) {
			
			List<Integer> left = new ArrayList<>();
			List<Integer> right = new ArrayList<>();
			
			int sumFromLeft = 0;			
			int hinge = 0;
			
			for(int j = 0; j < arr.size(); j++) {
				if(j < i) {
					left.add(arr.get(j));
					sumFromLeft += arr.get(j);
				} else if(j == i) {
					hinge = arr.get(j);
				} else {
					right.add(arr.get(j));
				}
			}
			
			int sumFromRight = sum - (sumFromLeft+hinge);
			
			if(sumFromLeft == sumFromRight) {
				System.out.println("Hinge detected at "+hinge);
				System.out.print(left);
				System.out.print(" "+hinge+" ");
				System.out.println(right);
				System.out.println("Left sum: "+sumFromLeft+" Right sum: "+sumFromRight);
				return "YES";
			}
		}
		return "NO";
    }

}