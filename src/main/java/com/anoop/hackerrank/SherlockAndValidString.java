package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sherlock considers a string to be valid if all characters of the string appear 
 * the same number of times. It is also valid if he can remove just 1 character 
 * at 1 index in the string, and the remaining characters will occur the same number 
 * of times. Given a string 's', determine if it is valid. 
 * If so, return YES, otherwise return NO.
 * Example:
 * s = abc
 * This is a valid string because frequencies are {a:1, b:1, c:1}.
 * s = abcc
 * This is a valid string because we can remove one 'c' and have 1 of each character 
 * in the remaining string.
 * s = abccc
 * This string is not valid as we can only remove 1 occurrence of 'c'. 
 * That leaves character frequencies of {a:1, b:1, c:2}.
 * 
 * @author ajaloree
 *
 */
public class SherlockAndValidString {

	public static String isValid(String s) {
		
		Map<Character, Integer> map = new HashMap<>();
		for(Character c : s.toCharArray()) {
			if(map.containsKey(c)) {
				map.put(c, map.get(c)+1);
			} else {
				map.put(c, 1);
			}
		}
		
		//{a=111, b=111, c=111, d=111, e=111, n=1}
		System.out.println(map);
		
		Map<Integer, List<Character>> freqMap = new HashMap<>();
		for(Character c : map.keySet()) {
			Integer cnt = map.get(c);
			if(freqMap.containsKey(cnt)) {
				freqMap.get(cnt).add(c);
			} else {
				List<Character> ls = new ArrayList<>();
				ls.add(c);
				freqMap.put(cnt, ls);
			}
		}
		
		//{1=[n], 111=[1, b, c, d, e]}
		System.out.println(freqMap);
		
		if(freqMap.size() == 1) {
			return "YES"; //All chars appear with same frequency
		} else if(freqMap.size() == 2) {
			
			List<Integer> counts = new ArrayList<>(freqMap.keySet());
			
			int count1 = counts.get(0);
			int numChars1 = freqMap.get(count1).size();
			int count2 = counts.get(1);
			int numChars2 = freqMap.get(count2).size();
			
			if(numChars1 == 1 || numChars2 == 1) {
				
				int errantCount = (numChars1 == 1) ? count1 : count2;
				int expectedCount = (numChars1 == 1) ? count2 : count1;
				
				if(errantCount == 1) {
					return "YES"; //Delete this character and rest of chars will appear with same frequency
				} else {
					//Return yes only when removal of one occurance of errant char will make frequency same
					return ((errantCount - expectedCount) == 1) ? "YES" : "NO";
				}
			} else {
				return "NO";
			}
			
		} else {
			return "NO"; //More than two chars appear with different frequency
		}
	}
	
}
