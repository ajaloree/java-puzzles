package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * There is a collection of input strings and a collection of query strings. 
 * For each query string, determine how many times it occurs in the list of 
 * input strings. Return an array of the results.
 * 
 * @author ajaloree
 *
 */
public class SparseArrays {

	public static List<Integer> matchingStrings(List<String> strings, List<String> queries) {
		
		List<Integer> ret = new ArrayList<>(queries.size());
		Map<String,Integer> counts = new HashMap<>();
		for(String item : strings) {
			counts.put(item, counts.getOrDefault(item, 0)+1);
		}
		for(String query : queries) {
			ret.add(counts.getOrDefault(query, 0));
		}
		
		return ret;
	}
	
}
