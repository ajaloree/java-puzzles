package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * There are two n-element arrays of integers, A and B. 
 * Permute them into some A' and B' such that the relation 
 * A'(i) + B'(i) >= k holds for all i where 0 <= i < n.
 * There will be q queries consisting of A, B, and k. 
 * For each query, return YES if some permutation A", B' 
 * satisfying the relation exists. Otherwise, return NO.
 * 
 * @author ajaloree
 *
 */
public class PermutingTwoArrays {

	public static String twoArrays(int k, List<Integer> A, List<Integer> B) {
	    
		String ret = "YES";

		A = A.stream().sorted().collect(Collectors.toList());
		B = B.stream().sorted().collect(Collectors.toList());
		
		List<Integer> permA = new ArrayList<>();
		List<Integer> permB = new ArrayList<>();
				
		for(int i = 0; i < A.size(); i++) {
			
			boolean matched = false;
			
			for(int j = 0; j < B.size(); j++) {

				if(A.get(i) + B.get(j) >= k) {
					matched = true;
					permA.add(A.get(i));
					permB.add(B.get(j));
					B.remove(j);
					break;
				}
			}
			if(!matched) {
				ret = "NO";
				break;
			}
		}
		
		if(ret.equals("YES")) {
			System.out.println(permA);
			System.out.println(permB);
		}
		
		return ret;
	}
}