package com.anoop.hackerrank;

public class ReverseDoublyLinkedList {
	
	public static class DoublyLinkedListNode {
	    public int data;
	    public DoublyLinkedListNode next;
	    public DoublyLinkedListNode prev;

	    public DoublyLinkedListNode(int nodeData) {
	        this.data = nodeData;
	        this.next = null;
	        this.prev = null;
	    }
	}
	
	public static class DoublyLinkedList {
		
	    public DoublyLinkedListNode head;
	    public DoublyLinkedListNode tail;

	    public DoublyLinkedList() {
	        this.head = null;
	        this.tail = null;
	    }

	    public void insertNode(int nodeData) {
	        DoublyLinkedListNode node = new DoublyLinkedListNode(nodeData);

	        if (this.head == null) {
	            this.head = node;
	        } else {
	            this.tail.next = node;
	            node.prev = this.tail;
	        }

	        this.tail = node;
	    }
	}

    public static String printForwardTraversal(DoublyLinkedListNode head) {
    	
    	StringBuilder sb = new StringBuilder();
    	DoublyLinkedListNode current = head;
    	while(current != null) {
    		sb.append(current.data+" -> ");
    		current = current.next;
    	}
    	sb.append("null");
    	return sb.toString();
    }

    public static String printBackwardTraversal(DoublyLinkedListNode tail) {
    	
    	StringBuilder sb = new StringBuilder();
    	DoublyLinkedListNode current = tail;
    	while(current != null) {
    		sb.append(current.data+" -> ");
    		current = current.prev;
    	}
    	sb.append("null");
    	return sb.toString();
    }

    public static DoublyLinkedListNode reverse(DoublyLinkedListNode llist) {
		
		DoublyLinkedListNode temp1 = null;
		DoublyLinkedListNode temp2 = null;
		
		DoublyLinkedListNode current = llist;
				
		while(current != null) {
			
			temp2 = current.next;
			
			current.next = temp1;
			if(temp1 != null) {
				temp1.prev = current;
			}
			temp1 = current;
			current = temp2; 
		}
		return temp1;
	}

}