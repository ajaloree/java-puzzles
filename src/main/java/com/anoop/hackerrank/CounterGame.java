package com.anoop.hackerrank;

/**
 * Louise and Richard have developed a numbers game. 
 * They pick a number and check to see if it is a power of 2. 
 * If it is, they divide it by 2. If not, they reduce it by the 
 * next lower number which is a power of 2. Whoever reduces the 
 * number to 1 wins the game. Louise always starts.
 * Given an initial value, determine who wins the game.
 * Example: n=132
 * It's Louise's turn first. She determines that 132 is not a power of 2. 
 * The next lower power of 2 is 128, so she subtracts that from 132 and 
 * passes 4 to Richard. 4 is a power of 2, so Richard divides it by 2 and 
 * passes 2 to Louise. Likewise, 2 is a power so she divides it by 2 and reaches 1. 
 * She wins the game.
 * @author ajaloree
 *
 */
public class CounterGame {

    public static String counterGame(long n) {
    	
    	int count = 1;
    	while(n != 1) {
    		if(isPowerOfTwo(n)) {
    			n = n / 2;
    		} else {
    			n = (n - nearestPowerOfTwo(n));
    		}
    		count++;
    	}
    	return (count%2==0) ? "Louise" : "Richard";
    }
    
    /**
     * All power of two numbers has only a one-bit set. 
     * So count the no. of set bits and if you get 1 then the number is a power of 2.
     */
    public static boolean isPowerOfTwo(long n) {
        return (Long.toBinaryString(n).chars().filter(c -> c=='1').count() == 1);
    }
    
    /**
     * Log of a number with base 2 will give the exponent/power to which 2 should be raised to
     * in order to get the number. In case of a decimal logarithm, just considering the integral
     * part will provide the minimum power of 2.
     */
    public static long nearestPowerOfTwo(long num) {
    	return (long)Math.pow(2,((int)log2(num)));
    }
    
    private static double log2(long n) {
    	return (Math.log(n) / Math.log(2));
    }
    
    // Alternate algorithm to find if a number is power of two - but is slow for big numbers.
	private static boolean isPowerOfTwoSlow(long num) {
		
		boolean isOn = true;
		int shift = 0;
		while(isOn) {
			long pow = (2 << shift);
			if(pow == num) {
				return true;
			} else if(pow > num){
				isOn = false;
			}
			shift++;
		}
		return false;
	}
	
    // Alternate algorithm to find nearest power of two - but is slow for big numbers.
	private static long nearestPowerOfTwoSlow(long num) {

		boolean isOn = true;
		long nearestPowOf2 = 0;
		int count = 1;
		
		while(isOn) {
			long pow = (2 << count);
			if(pow < num) {
				nearestPowOf2 = pow;
			} else {
				isOn = false;
			}
			count++;
		}
		
		return nearestPowOf2;
	}
	
}