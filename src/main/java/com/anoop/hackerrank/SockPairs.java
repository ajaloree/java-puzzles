package com.anoop.hackerrank;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * There is a large pile of socks that must be paired by color. 
 * Given an array of integers representing the color of each sock, 
 * determine how many pairs of socks with matching colors there are.
 * 
 * @author ajaloree
 */
public class SockPairs {
	
	public static int sockMerchant(List<Integer> ar) {
	 
		Map<Integer,List<Integer>> grouped = ar.stream().collect(Collectors.groupingBy(v -> v));
		AtomicReference<Integer> totalPairs = new AtomicReference<Integer>(0);
		grouped.forEach((k,v) -> {
			totalPairs.set(totalPairs.get()+(v.size()/2));
		});
		return totalPairs.get();
	}

}
