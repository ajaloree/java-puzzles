package com.anoop.hackerrank;

/**
 * Given pointers to the heads of two sorted linked lists, merge them 
 * into a single, sorted linked list. Either head pointer may be null 
 * meaning that the corresponding list is empty.
 * 
 * Example:
 * headA refers to 1->3->7->null 
 * headB refers to 1->2->null 
 * The new list is 
 * 1->1->2->3->7->null
 * 
 * @author ajaloree
 *
 */
public class MergeLists {

	public static class SinglyLinkedListNode {
		
	    public int data;
	    public SinglyLinkedListNode next;

	    public SinglyLinkedListNode(int nodeData) {
	        this.data = nodeData;
	        this.next = null;
	    }
	}

    public static class SinglyLinkedList {
        
    	public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;

        public SinglyLinkedList() {
            this.head = null;
            this.tail = null;
        }

        public void insertNode(int nodeData) {
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

            if (this.head == null) {
                this.head = node;
            } else {
                this.tail.next = node;
            }

            this.tail = node;
        }
    }
    
    public static String printForwardTraversal(SinglyLinkedListNode head) {
    	
    	StringBuilder sb = new StringBuilder();
    	SinglyLinkedListNode current = head;
    	while(current != null) {
    		sb.append(current.data+" -> ");
    		current = current.next;
    	}
    	sb.append("null");
    	return sb.toString();
    }

	public static SinglyLinkedListNode mergeLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
		
		if(head1 == null) {
			return head2;
		} 
		if(head2 == null) {
			return head1;
		}
		
		SinglyLinkedListNode nodeL1 = head1;
		SinglyLinkedListNode nodeL2 = head2;
		SinglyLinkedList merged = new SinglyLinkedList();
		
		while(nodeL1 != null && nodeL2 != null) {
			
			if(nodeL1.data <= nodeL2.data) {
				merged.insertNode(nodeL1.data);
				nodeL1 = nodeL1.next;
			} else {
				merged.insertNode(nodeL2.data);
				nodeL2 = nodeL2.next;
			}
		}

		if(nodeL1 != null) {
			while(nodeL1 != null) {
				merged.insertNode(nodeL1.data);
				nodeL1 = nodeL1.next;
			}
		} else {
			while(nodeL2 != null) {
				merged.insertNode(nodeL2.data);
				nodeL2 = nodeL2.next;
			}

		}
		
		return merged.head;
    }
}