package com.anoop.hackerrank;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Counting sort does not require comparison for sorting. 
 * Instead, you create an integer array whose index range covers the entire range of values 
 * in your array to sort. Each time a value occurs in the original array, you increment the 
 * counter at that index. At the end, run through your counting array, printing the value of 
 * each non-zero valued index that number of times.
 * 
 * @author ajaloree
 *
 */
public class CountingSort {

	public static List<Integer> countingSort(List<Integer> arr) {
		
		int[] freqArr = new int[100];
		arr.forEach(e -> {
			freqArr[e]++;
		});
		
		return Arrays.stream(freqArr).boxed().collect(Collectors.toList());
	}
}
