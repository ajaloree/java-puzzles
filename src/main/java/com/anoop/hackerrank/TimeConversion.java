package com.anoop.hackerrank;

/**
 * Given a time in -hour AM/PM format, convert it to military (24-hour) time.
 * Note: - 12:00:00AM on a 12-hour clock is 00:00:00 on a 24-hour clock.
 *       - 12:00:00PM on a 12-hour clock is 12:00:00 on a 24-hour clock.
 * @author ajaloree
 *
 */
public class TimeConversion {
	
	public static String timeConversion(String s) {

		String[] splits = s.split(":");
		
		boolean isAM = splits[2].endsWith("AM")?true:false;

		String hh = splits[0];
		String mm = splits[1];
		String ss = splits[2].substring(0, 2);
		
		String ret = hh+":"+mm+":"+ss;
		
		if(isAM) {
			if(Integer.parseInt(hh) == 12) {
				ret = "00:"+mm+":"+ss;
			} 
		} else {
			if(Integer.parseInt(hh) != 12) {
				ret = (Integer.parseInt(hh)+12)+":"+mm+":"+ss;
			} 
		}
		
		return ret;
	}
}
