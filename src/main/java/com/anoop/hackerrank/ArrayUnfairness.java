package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * You will be given a list of integers, arr, and a single integer k. 
 * You must create an array of length k from elements of arr such that 
 * its unfairness is minimized. Call that array arr'. Unfairness of an 
 * array is calculated as:
 * max(arr') - min(arr')
 * Where:
 * - max denotes the largest integer in arr' 
 * - min denotes the smallest integer in arr'
 * Note: Integers in  may not be unique.
 * @author ajaloree
 *
 */
public class ArrayUnfairness {
	
	/**
	 * Improvement over the slow implementation as it simply find the 
	 * first and last element of the sublist which will give min and 
	 * max elemnts as the list is pre-sorted.  
	 * @param arr
	 * @param k
	 * @return
	 */
	public static int maxMin(List<Integer> arr, int k) {
		
		if(arr.isEmpty()) return 0;
		if(k > arr.size()) return 0;
		
		int minUnfairness = Integer.MAX_VALUE;
		List<Integer> sorted = arr.stream().sorted().collect(Collectors.toList() );
		for(int i = 0; i < sorted.size()-k+1; i++) {
			int lo = sorted.get(i);
			int hi = sorted.get(i+k-1);
			int subArrUnfairness = hi - lo;
			if(subArrUnfairness < minUnfairness) {
				minUnfairness = subArrUnfairness;
			}
		}
		return minUnfairness;
	}
	
	/**
	 * Sub-optimal implementation as this algorithm creates a new list each time
	 * with k elements and then finds max and min from the sub-list, which is waste
	 * of space and implicit iterations while finding max and min of sub-lists.  
	 * @param arr
	 * @param k
	 * @return
	 */
	public static int maxMinSlow(List<Integer> arr, int k) {
		
		if(arr.isEmpty()) return 0;
		if(k > arr.size()) return 0;
		
		int minUnfairness = Integer.MAX_VALUE;
		List<Integer> sorted = arr.stream().sorted().collect(Collectors.toList() );
		for(int i = 0; i < sorted.size()-k+1; i++) {
			List<Integer> subList = new ArrayList<>();
			for(int j = i; j < (i+k); j++) {
				subList.add(sorted.get(j));
			}
			int subArrUnfairness = unfairness(subList);
			if(subArrUnfairness < minUnfairness) {
				minUnfairness = subArrUnfairness;
			}
			System.out.print("Sub array: "+subList);
			System.out.println("Unfairness: "+unfairness(subList));
		}
		return minUnfairness;
	}
	
	private static int unfairness(List<Integer> arr) {
		return arr.get(arr.size()-1) - arr.get(0);
	}

}