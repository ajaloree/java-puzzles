package com.anoop.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Given a square grid of characters in the range ascii[a-z], 
 * rearrange elements of each row alphabetically, ascending. 
 * Determine if the columns are also in ascending alphabetical 
 * order, top to bottom. Return YES if they are or NO if they are not.
 * Example
 * The grid is illustrated below.
 * a b c
 * a d e
 * e f g
 * The rows are already in alphabetical order. 
 * The columns a a e, b d f and c e g are also in alphabetical order, 
 * so the answer would be YES. 
 * Only elements within the same row can be rearranged. 
 * They cannot be moved to a different row.
 * 
 * @author ajaloree
 *
 */
public class GridChallenge {
	
	public static String gridChallenge(List<String> grid) {

		List<String> sortedGrid = sortGridRows(grid);
		
		System.out.println("Input: "+grid);
		System.out.println("Output: "+sortedGrid);
		
		return areColumnsSorted(sortedGrid);
	}
	
	private static List<String> sortGridRows(List<String> grid) {

		List<String> sortedGrid = new ArrayList<>(grid.size()); 
		for(String row : grid) {
			char[] arr = row.toCharArray();
			Arrays.sort(arr);
			String sorted = new String(arr);
			sortedGrid.add(sorted);
		}
		return sortedGrid;
	}
	
	private static String areColumnsSorted(List<String> grid) {

		int ROWS = grid.size();
		int COLS = grid.get(0).length();
		
		for(int col=0; col < COLS; col++) {
			for(int row = 0; row < ROWS-1; row++) {
				if(grid.get(row).charAt(col) > grid.get(row+1).charAt(col)) {
					return "NO";
				}
			}
		}
		return "YES";
	}
	
	public static void main(String[] args) {
		
		List<String> list = Stream.of("acbwdfg","daetgvh","hdzrjyr").collect(Collectors.toList());
		gridChallenge(list);
		
		System.out.println();
		
	}
}
