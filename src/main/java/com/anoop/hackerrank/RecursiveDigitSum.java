package com.anoop.hackerrank;

import java.util.stream.Stream;

public class RecursiveDigitSum {

    public static int superDigit(String n, int k) {
    	
    	if(n.isBlank()) return 0;
    	
    	/**
    	 * Below line is very important - this is needed when either n or k is very large.
    	 * It sums the digits of the number n and the the compressed number of catenated k
    	 * times, which doesnt interfere in the final answer as addition is associative.
    	 */
    	String x = sumOfDigits(n.trim());
    	
    	StringBuilder num = new StringBuilder();
    	Stream.iterate(0, m -> m+1).limit(k).forEach(i -> num.append(x));
    	String sum = sumOfDigits(num.toString());
    	while(sum.length() != 1) {
    		sum = sumOfDigits(sum);
    	}
    	return Integer.parseInt(sum);
    }

    private static String sumOfDigits(String num) {

    	long sum = 0;
    	for(char digit : num.toCharArray()) {
    		sum += Integer.parseInt(digit+"");
    	}
    	return Long.toString(sum);
    }
    
    /**
     * Sum of digits using long data type results in overflow
     * and is not suitable for large inputs (either n or k).
     */
    private static String sumOfDigits(long num) {

    	long sum = 0, n = num;
    	while(n != 0) {
    		sum += (n % 10);
    		n = n / 10;
    	}
    	return Long.toString(sum);
    }

}