package com.anoop.streams;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;
import static java.util.Comparator.comparingInt;

public class CollectorsDemo {

	public enum CaloricLevel { DIET, NORMAL, FAT }

	public static void main(String[] args) {

		List<Dish> menu = Arrays.asList(
				new Dish("pork", false, 800, Dish.Type.MEAT),
				new Dish("beef", false, 700, Dish.Type.MEAT),
				new Dish("chicken", false, 400, Dish.Type.MEAT),
				new Dish("french fries", true, 530, Dish.Type.OTHER),
				new Dish("rice", true, 350, Dish.Type.OTHER),
				new Dish("season fruit", true, 120, Dish.Type.OTHER),
				new Dish("pizza", true, 550, Dish.Type.OTHER),
				new Dish("prawns", false, 300, Dish.Type.FISH),
				new Dish("salmon", false, 450, Dish.Type.FISH));

		// Reducing and summarising collectors
		
        int totalCalories = menu.stream().collect(summingInt(Dish::getCalories));
		System.out.println(String.format("Total calories in menu: %d", totalCalories));
		
		int totalCaloriesUsingReducingColl = menu.stream().collect(reducing(0, Dish::getCalories, (x,y)->x+y));
		System.out.println(String.format("(again) Total calories in menu: %d", totalCaloriesUsingReducingColl));
		
        double avgCalories = menu.stream().collect(averagingDouble(Dish::getCalories));
		System.out.println(String.format("Average calories in a dish within menu: %.2f", avgCalories));
		
		IntSummaryStatistics menuStatistics = menu.stream().collect(summarizingInt(Dish::getCalories));
		System.out.println(String.format("Menu stats: %s", menuStatistics));

		String shortMenu = menu.stream().map(Dish::getName).collect(joining(", "));
		System.out.println(String.format("Short menu: %s", shortMenu));
		
		Optional<Dish> mostCalorieDish = menu.stream().collect(reducing((d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
		System.out.println(String.format("Most calorie dish: %s", mostCalorieDish));
		
		// Grouping collectors
		
		Map<Dish.Type, List<Dish>> dishesByType = menu.stream().collect(groupingBy(Dish::getType));
		System.out.println(String.format("Dishes by type: %s", dishesByType));
		
		Map<CaloricLevel, List<Dish>> dishesByCaloriecLevel = menu.stream().collect(groupingBy(d -> {
			if(d.getCalories() <= 400) return CaloricLevel.DIET;
			else if(d.getCalories() <= 700) return CaloricLevel.NORMAL;
			else return CaloricLevel.FAT;
		}));
		System.out.println(String.format("Dishes by caloric level: %s",dishesByCaloriecLevel));
		
		/**
		 * Below will produce empty value for a key - FISH - which would otherwise not have been possible
		 * if filter() was performed before collect()
		 */
		Map<Dish.Type, Set<Dish>> caloricDishesByType = menu.stream().collect(
															groupingBy(Dish::getType, 
																	   filtering(dish -> dish.getCalories() > 500, toSet())));
		System.out.println(String.format("Above average calorific dishes by type: %s", caloricDishesByType));
	
		// Partitioning
		
		Map<Boolean, List<Dish>> partitionedDishes = menu.stream().collect(partitioningBy(Dish::isVegetarian));
		System.out.println(String.format("Partitioned Dishes: %s", partitionedDishes));
		
		// same output as above for veggie dishes
		List<Dish> vegDishes = menu.stream().filter(Dish::isVegetarian).collect(toList());
		System.out.println(String.format("Veg Dishes: %s", vegDishes));
		
		// Partitioning and then grouping by type within each partition
		Map<Boolean, Map<Dish.Type,List<Dish>>> subPartDishes = menu.stream().collect(partitioningBy(Dish::isVegetarian, groupingBy(Dish::getType)));
		System.out.println(subPartDishes);
		
		// To find most caloric dish among vegetarian and non-vegetarian categories
		Map<Boolean, Dish> mostCaloricPartitionedByVegetarian =
				menu.stream().collect(
				    partitioningBy(Dish::isVegetarian,
				         collectingAndThen(maxBy(comparingInt(Dish::getCalories)),
				                           Optional::get)));
		System.out.println(mostCaloricPartitionedByVegetarian);
		
		int N = 100;
		// Partition first n numbers into prime and non-prime
		Map<Boolean, List<Integer>> nums  = IntStream.rangeClosed(2, N).boxed().collect(partitioningBy(i -> isPrime(i)));
		System.out.println("Prime and Non-Prime nums: "+nums);
		
	}

	public static boolean isPrime(int num) {
		int numRoot = (int) Math.sqrt((double)num);
		return IntStream.rangeClosed(2, numRoot).noneMatch(i -> num%i==0);
	}

}
