package com.anoop.streams;

import java.util.stream.LongStream;
import java.util.stream.Stream;

public class ParallelStreamDemo {
	
	public static void main(String[] args) {
		
		System.out.println(sequentialSum(1000000L));
		System.out.println(parallelSumInefficient(1000000L));
	}
	
	public static Long sequentialSum(Long n) {
		return Stream.iterate(1L, i->i+1).limit(n).reduce(0L, Long::sum);
	}
	
	/**
	 * Two issues are mixed together:
	 * - iterate generates boxed objects, which have to be unboxed to numbers before they can be added.
	 * - iterate is difficult to divide into independent chunks to execute in parallel.
	 * @param n
	 * @return
	 */
	public static Long parallelSumInefficient(Long n) {
		return Stream.iterate(1L, i->i+1)
				     .limit(n)
				     .parallel()
				     .reduce(0L, Long::sum);
	}
	
	/**
	 * Efficient than above function because of two reasons:
	 * - LongStream.rangeClosed works on primitive long numbers directly so there’s no boxing and unboxing overhead.
	 * - LongStream.rangeClosed produces ranges of numbers, which can be easily split into independent chunks.
	 * The numeric stream is much faster than the earlier sequential version, generated with the iterate factory method, 
	 * because the numeric stream avoids all the overhead caused by all the unnecessary autoboxing and auto-unboxing 
	 * operations performed by the nonspecialized stream. This is evidence that choosing the right data structures is 
	 * often more important than parallelizing the algorithm that uses them.
	 * @param n
	 * @return
	 */
	public static Long parallelEfficient(Long n) {
		return LongStream.rangeClosed(1, n).reduce(0L, Long::sum);
	}

}
