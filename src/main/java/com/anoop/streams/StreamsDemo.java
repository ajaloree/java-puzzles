package com.anoop.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsDemo {

	public static void main(String[] args) {

		System.out.println(generateNumsFrom1toNandIgnoreFirstMelementsFromHeadAndTail(10,3));
		System.out.println(generateNumsFrom1toNandIgnoreFirstMelementsFromHeadAndTail(100,10));

		System.out.println(getListOfDistinctAlphabetsInStrings(List.of("anshuman", "anoop")));
		
		System.out.println(isAllEven(List.of(2,4,5,6,8)));		
		System.out.println(isAnyEven(List.of(2,4,5,6,8)));
		System.out.println(isNoneEven(List.of(1,3,5,7,9)));

	}
	
	public static List<Integer> generateNumsFrom1toNandIgnoreFirstMelementsFromHeadAndTail(int N, int M) {
		return Stream.iterate(1, x->x+1)
				     .skip(M)
				     .limit(N-2*M)
				     .collect(Collectors.toList());
	}
	
	public static List<String> getListOfDistinctAlphabetsInStrings(List<String> words) {
		return words.stream()
				 	.map(w -> w.split("")) //Converts each word into an array of its individual letters
				 	.flatMap(Arrays::stream)//Makes each array into a separate stream and flattens each generated stream into a single stream
				 	.distinct()
				 	.collect(Collectors.toList());
	}
	
	public static boolean isAnyEven(List<Integer> nums) {
		return nums.stream().anyMatch(n -> n%2==0);
	}

	public static boolean isAllEven(List<Integer> nums) {
		return nums.stream().allMatch(n -> n%2==0);
	}

	public static boolean isNoneEven(List<Integer> nums) {
		return nums.stream().noneMatch(n -> n%2==0);
	}
	
	public static boolean isPrime(int num) {
		int numRoot = (int) Math.sqrt((double)num);
		return IntStream.rangeClosed(2, numRoot).noneMatch(i -> num%i==0);
	}

}
