package com.anoop.leetcode;

import java.util.Arrays;

/**
 * Given the array with 'n' elements, rotate the array 
 * to the right by 'k' steps, where 'k' is non-negative.
 * 
 * O(1) Solution Algorithm:
 * [Step-1] Reverse the array.
 * [Step-2] Reverse the sub-array of the array consisting of first 'k' elements.
 * [Step-3] Reverse the rest of the array excluding the front sub-array.
 * 
 * @author ajaloree
 *
 */
public class ArrayRotation {
	
	public static void rotate(int[] arr, int k) {
		
		//Step-1: Reverse the array
		reverse(arr, 0, arr.length-1);
		
		//Step-2: Reverse sub-array with first 'k' elements
		reverse(arr, 0, k-1);
		
		//Step-3: Reverse rest of the sub-array
		reverse(arr, k, arr.length-1);
		
	}
	
	private static void reverse(int[] arr, int lo, int hi) {
		
		if(arr.length == 1) {
			return;
		}
		if(arr.length == 2) {
			swap(arr,0,1);
			return;
		}
		
		System.out.println(Arrays.toString(arr));
		int lim = lo + (hi-lo)/2;
		while(lo < lim) {
			swap(arr, lo, hi);
			lo++;
			hi--;
		}
	}
	
	private static void swap(int[] arr, int x, int y) {
		int temp = arr[x];
		arr[x] = arr[y];
		arr[y] = temp;
	}

}
