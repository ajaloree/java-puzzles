package com.anoop.list.ds;

public class ListNode<T> {
	
	public T item;
	public ListNode<T> next;
	
	public ListNode(T item) {
		this.item = item;
	}

}