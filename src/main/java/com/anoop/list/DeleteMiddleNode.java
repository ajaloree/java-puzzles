package com.anoop.list;
/**
 * Implement an algorithm to delete a node in the middle (i.e. any node but the first and last 
 * node, not necessarily the exact middle) of a singly linked list, given only access to that node. 
 * 
 * Example:
 * 
 * Input: The node 'c' from the linked list a -> b -> c -> d -> e -> f
 * Result: Nothing is returned, but the new linked list looks like:
 *         a -> b -> d -> e -> f
 * 
 * @author ajaloree
 *
 */

import com.anoop.list.ds.ListNode;

public class DeleteMiddleNode {
	
	/**
	 * In this problem, you are not given access to the head of the linked list.
	 * You only have access to the node that needs to be deleted. The solution is
	 * to copy over the data from the next node to the current node, and then to 
	 * delete the next node.
	 */
	public static <T> boolean deleteNode(ListNode<T> n) {
		
		if(n == null || n.next == null) return false;
		
		ListNode<T> next = n.next;
		n.item = next.item;
		n.next = next.next;
		return true;
	}
	
}
