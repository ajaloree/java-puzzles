package com.anoop.list;

import com.anoop.list.ds.ListNode;

public class ReverseList {

	/**
	 * Recursive solution: Assuming the linked list has N nodes, we recursively
	 * reverse the last N – 1 nodes, and then carefully append the first node to the end.
	 * 
	 * @param <T>
	 * @param first
	 * @return
	 */
	public static <T> ListNode<T> reverseRecursive(ListNode<T> first) {
		
		if (first == null) return null;
	    if (first.next == null) return first;
	    ListNode<T> second = first.next;
	    ListNode<T> rest = reverseRecursive(second);
	    second.next = first;
	    first.next  = null;
	    return rest;
	}
	
	/**
	 * Iterative solution : To accomplish this task, we maintain references to three 
	 * consecutive nodes in the linked list, reverse, first, and second. At each iteration, 
	 * we extract the node first from the original linked list and insert it at the beginning 
	 * of the reversed list. We maintain the invariant that first is the first node of what’s 
	 * left of the original list, second is the second node of what’s left of the original list, 
	 * and reverse is the first node of the resulting reversed list.
	 * 
	 * @param <T>
	 * @param x
	 * @return
	 */
	public static <T> ListNode<T> reverseIterative(ListNode<T> x) {

		ListNode<T> first = x;
		ListNode<T> reverse = null;
		while (first != null) {
			ListNode<T> second = first.next;
			first.next = reverse;
			reverse = first;
			first = second;
		}
		return reverse;
	}
}
