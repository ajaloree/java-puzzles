package com.anoop.list;

import java.util.HashSet;
import java.util.Set;

import com.anoop.list.ds.ListNode;

public class RemoveDuplicates {

	public static <T> void removeDuplicatesUsingBuffer(ListNode<T> n) {
		
		Set<T> set = new HashSet<>();
		ListNode<T> previous = null;
		while(n != null) {
			if(set.contains(n.item)) {
				previous.next = n.next;
			} else {
				set.add(n.item);
				previous = n;
			}
			n = n.next;
		}
	}

	public static <T> void removeDuplicatesWithoutBuffer(ListNode<T> n) {
		
		ListNode<T> current = n;
		while(current != null) {
			ListNode<T> runner = current;
			while(runner.next != null) {
				if(runner.next.item.equals(current.item)) {
					runner.next = runner.next.next;
				} else {
					runner = runner.next;
				}
			}
			current = current.next;
		}
	}

	public static void main(String[] args) {
		
		ListNode<String> a = new ListNode<String>("a"); 
		ListNode<String> b = new ListNode<String>("b");
		ListNode<String> c1 = new ListNode<String>("c");
		ListNode<String> c2 = new ListNode<String>("c");
		ListNode<String> d = new ListNode<String>("d");
		ListNode<String> e = new ListNode<String>("e");
		ListNode<String> f1 = new ListNode<String>("f");
		ListNode<String> f2 = new ListNode<String>("f");
		a.next = b;
		b.next = c1;
		c1.next = c2;
		c2.next = d;
		d.next = e;
		e.next = f1;
		f1.next = f2;
		
		removeDuplicatesWithoutBuffer(a);

		
	}
}