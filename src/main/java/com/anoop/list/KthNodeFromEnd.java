package com.anoop.list;

import com.anoop.list.ds.ListNode;

/**
 * Algorithm to find Kth element from the end of a singly linked list.
 * 
 * @author ajaloree
 *
 */
public class KthNodeFromEnd {
	
	/**
	 * 
	 * @param <T>
	 * @param k
	 * @param head
	 * @return
	 */
	public static <T> ListNode<T> elementFromEnd(int k, ListNode<T> head) {
		
		ListNode<T> p1 = head;
		ListNode<T> p2 = head;
		
		// move p1 k nodes into the list
		for(int i = 0; i < k; i++) {
			if(p1 == null) return null;
			p1 = p1.next;
		}
		
		// move both p1 and p2 at the same pace. When p1 hits the end, p2 will be at the right elememt
		while(p1 != null) {
			p1 = p1.next;
			p2 = p2.next;
		}
		
		return p2;
	}

}
