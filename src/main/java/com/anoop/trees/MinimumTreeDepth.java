package com.anoop.trees;

import com.anoop.trees.ds.IntNode;

/**
 * Find the minimum depth of a binary tree
 * Given a binary tree, find its minimum depth. 
 * The minimum depth is the total number of nodes along the 
 * shortest path from the root node down to the nearest leaf node.
 * For example, the minimum depth of the following binary tree is 2. 
 * The shortest path is 1 —> 3 —> 6
 * 
 * 							1
 * 						   / \
 *                        /   \
 *                       2     3
 *                      / \   / \
 *                     4   5 6   7
 *                      \   \   / \
 *                       8   9 10  11
 * 
 * @author ajaloree
 *
 */
public class MinimumTreeDepth {

	public static int minimumDepth(IntNode node) {
		
		if(node == null) {
			return 0;
		} else if(node.left == null && node.right == null) {
			return 0;
		} else {
			return Math.min(minimumDepth(node.left), minimumDepth(node.right)) + 1;
		}
	}
	
}
