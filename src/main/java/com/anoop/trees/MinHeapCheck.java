package com.anoop.trees;

import com.anoop.trees.ds.IntNode;

/**
 * Check if a binary tree is a min-heap or not.
 * Given a binary tree, check if it is a min-heap or not. 
 * In order words, the binary tree must be a complete binary 
 * tree where each node has a higher value than its parent’s value.
 * For example, the following binary tree is a min-heap:
 * 
 * 								2
 * 							   / \
 *   						  /   \
 *    						 3     4
 *    						/ \   / \
 *                         5   6 7   8
 * 
 * On the other hand, the following binary tree is not a min-heap:
 * 
 * 								5
 * 							   / \
 *   						  /   \
 *    						 3     8
 *    						/ \   / \
 *                         2   4 6   10

 * @author ajaloree
 *
 */
public class MinHeapCheck {

	private static boolean isMinHeapNode(IntNode node) {
		
		if(node == null) {
			return true;
		} else if(node.left != null && node.data > node.left.data) {
			return false;
		} else if(node.right != null && node.data > node.right.data) {
			return false;
		} else {
			return true;
		}
	}
	
	public static boolean isMinHeapTree(IntNode root) {
		
		return isMinHeapNode(root) && isMinHeapNode(root.left) && isMinHeapNode(root.right);
	}
}