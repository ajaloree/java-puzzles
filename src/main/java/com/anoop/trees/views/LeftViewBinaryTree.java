package com.anoop.trees.views;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.anoop.trees.ds.IntNode;

/**
 * Given a binary tree, print it's left view. 
 * Assume the left and right child of a node makes a 45–degree angle with the parent.
 * For example, the left view of the following tree is 1, 2, 4, 7.
 * 
 * 						1
 * 					   / \
 *                    /   \
 *                   /     \
 *                  2       3
 *                   \     / \
 *                    4   5   6
 *                       / \
 *                      /   \
 *                     7     8
 * 
 * @author ajaloree
 *
 */
public class LeftViewBinaryTree {
	
	public static List<List<IntNode>> getNodesByLevel(IntNode root) {
		
		List<List<IntNode>> nodesByLevel = new ArrayList<>();
		if(root != null) {
			
			Queue<IntNode> queue = new LinkedList<>();
			queue.add(root);
			while(!queue.isEmpty()) {
				
				List<IntNode> nodes = new ArrayList<>();
				while(!queue.isEmpty()) {
					nodes.add(queue.remove());
				}
				nodesByLevel.add(nodes);
				for(IntNode node : nodes) {
					if(node.left != null) queue.add(node.left);
					if(node.right != null) queue.add(node.right);
				}
			}
		}
		System.out.println(nodesByLevel);
		return nodesByLevel;
	}
	
	public static List<IntNode> getLeftTree(IntNode root) {
		
		List<IntNode> leftView = new ArrayList<>();
		List<List<IntNode>> nodesByLevel = getNodesByLevel(root);
		for(List<IntNode> nodes : nodesByLevel) {
			leftView.add(nodes.get(0));
		}
		return leftView;
	}

}