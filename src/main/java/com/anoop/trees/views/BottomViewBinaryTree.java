package com.anoop.trees.views;

import java.util.Map;
import java.util.TreeMap;

import com.anoop.trees.ds.IntNode;
/**
 * Given a binary tree, print the bottom view of it. 
 * Assume the left and right child of a node makes a 45–degree angle with the parent.
 * For example, the top view of the following tree is 7, 5, 8, 6:
 * 
 * 						1
 * 					   / \
 *                    /   \
 *                   2     3
 *                        / \
 *                       /   \
 *                      5     6
 *                     / \
 *                    /   \
 *                   7     8
 * 
 * @author ajaloree
 *
 */
public class BottomViewBinaryTree {
	
	private static void traverse(IntNode node, Map<Integer, Integer> accumulator, int abscissa) {
		
		if(node == null) {
			return;
		}
		
		accumulator.put(abscissa, node.data);
		
		traverse(node.left, accumulator, abscissa-1);
		
		traverse(node.right, accumulator, abscissa+1);
	}
	
	private static void traverseFromRoot(IntNode root, Map<Integer, Integer> accumulator) {
		traverse(root, accumulator, 0);
	}	
	
	public static Map<Integer, Integer> getBottomView(IntNode root) {
		Map<Integer, Integer> accumulator = new TreeMap<>();
		traverseFromRoot(root, accumulator);
		System.out.println(accumulator);
		return accumulator;
	}
}