package com.anoop.trees;

import com.anoop.trees.ds.StringNode;

/**
 * Given a binary tree, write an efficient algorithm to check if it has a 
 * symmetric structure or not, i.e., left and right subtree mirror each other.
 * 
 * Example-1:
 *           a               
 *          / \
 *         b   c
 *         \   /
 *          d e
 *         
 * Example-2:
 * 
 *          a
 *         / \
 *        b   c
 *       /     \
 *      d       e
 *      
 * 
 * Example-3:
 *          a
 *         / \
 *        b   c
 *       /     \
 *      d       e
 *      \       /
 *       f     g
 *        
 * @author ajaloree
 *
 */
public class SymmetricTreeCheck {
	
	private static boolean areNodesSymmetric(StringNode n1, StringNode n2) {
		
		if(n1 == null && n2 == null) {
			return true;
		} else {
			return (n1 != null && n2 != null) && 
					areNodesSymmetric(n1.left, n2.right) && 
					areNodesSymmetric(n1.right, n2.left);
		}
	}

	public static boolean isTreeSymmetric(StringNode root) {
		
		return areNodesSymmetric(root.left, root.right);
		
	}

	//Function is logically correct - but more complex. Favour areNodesSymmetric()
	@SuppressWarnings("unused")
	private static boolean areNodesSymmetricInefficient(StringNode n1, StringNode n2) {
		
		if(n1 == null && n2 == null) {
			return true;
		} else if (n1 == null ^ n2 == null) {
			return false;
		} else if(n1.left == null && n1.right == null && n2.left == null && n2.right == null) {
			return true;
		} else if(n1.left == null && n1.right != null && n2.left != null && n2.right == null) {
			return true && areNodesSymmetric(n1.right, n2.left);
		} else if(n1.left != null && n1.right == null && n2.left == null && n2.right != null) {
			return true && areNodesSymmetric(n1.left, n2.right);
		} else if(n1.left != null && n1.right != null && n2.left != null && n2.right != null) {
			return true && areNodesSymmetric(n1.left, n2.right) && areNodesSymmetric(n1.right, n2.left);
		} else {
			return false;
		}
	}
}