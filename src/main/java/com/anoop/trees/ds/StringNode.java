package com.anoop.trees.ds;

public class StringNode {

	public final String data;
	
	public StringNode(String data) {
		this.data = data;
	}
	
	public StringNode left;
	public StringNode right;
	
	@Override
	public String toString() {
		return "("+data+")";
	}

}