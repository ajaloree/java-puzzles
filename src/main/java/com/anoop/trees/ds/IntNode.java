package com.anoop.trees.ds;

public class IntNode {

	public final Integer data;
	
	public IntNode(Integer data) {
		this.data = data;
	}
	
	public IntNode left;
	public IntNode right;
	
	@Override
	public String toString() {
		return "("+data+")";
	}
}