package com.anoop.trees;

import java.util.Map;
import java.util.TreeMap;

import com.anoop.trees.ds.IntNode;

/**
 * Given a binary tree, the print vertical sum of it. 
 * Assume the left and right child of a node makes a 
 * 45–degree angle with the parent.
 * 
 * Examples:
 * 
 * 						5
 * 					   / \
 *                    /   \
 *                   7     8
 *                  /       \
 *                 /         \
 *                9           2
 *               / \         / \
 *              /   \       /   \
 *             3     4     6     10
 *       ---------------------------------
 *             3  9  11 5 14  2  10
 *       ---------------------------------      
 *       
 *                     1
 *                    / \
 *                   /   \
 *                  2     3
 *                       / \
 *                      /   \
 *                     5     6
 *                    / \
 *                   /   \
 *                  7     8
 *            ----------------------
 *                  9  6  11 6
 *            ----------------------      
 *                  
 * @author ajaloree
 *
 */
public class BinaryTreeVerticalSum {

	public static void traverseNode(IntNode node, int level, Map<Integer, Integer> accumulator) {
		
		accumulator.put(level, accumulator.getOrDefault(level, 0)+node.data);
		if(node.left != null) {
			traverseNode(node.left, level-1, accumulator);
		}
		if(node.right != null) {
			traverseNode(node.right, level+1, accumulator);
		}
	}
	
	public static Map<Integer, Integer> getVerticalSum(IntNode root) {
		Map<Integer, Integer> accumulator = new TreeMap<>();
		traverseNode(root, 0, accumulator);
		return accumulator;
	}

}