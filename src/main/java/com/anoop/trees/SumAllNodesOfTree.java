package com.anoop.trees;

import com.anoop.trees.ds.IntNode;

/**
 * Summ all nodes of a binary tree.
 *  
 * @author ajaloree
 *
 */
public class SumAllNodesOfTree {

	public static int sumFrom(IntNode node) {
		
		if(node == null) {
			return 0;
		} else if(node.left == null && node.right == null) {
			return node.data;
		} else {
			return node.data + sumFrom(node.left) + sumFrom(node.right);
		}
	}
}