package com.anoop.trees;

import com.anoop.trees.ds.IntNode;

/**
 * Check if a binary tree is a sum tree or not
 * Given a binary tree, check if it is a sum tree or not. In a sum tree, 
 * each non-leaf node’s value is equal to the sum of all elements present 
 * in its left and right subtree. The value of a leaf node can be anything 
 * and the value of an empty child node is considered to be 0.
 * 
 * @author ajaloree
 *
 */
public class SumTreeCheck {

	public static boolean isSumNode(IntNode node) {
		
		if(node == null) {
			return true;
		} else {
			return node.data == sumFrom(node.left) + sumFrom(node.right);
		}
	}
	
	private static int sumFrom(IntNode node) {
		
		if(node == null) {
			return 0;
		} else if(node.left == null && node.right == null) {
			return node.data;
		} else {
			return node.data + sumFrom(node.left) + sumFrom(node.right);
		}
	}

	
	public static void main(String[] args) {
		
		
		IntNode root = new IntNode(44);
		IntNode a = new IntNode(9);
		IntNode b = new IntNode(13);
		IntNode c = new IntNode(4);
		IntNode d = new IntNode(5);
		IntNode e = new IntNode(6);
		IntNode f = new IntNode(7);

		root.left = a;
		root.right = b;
		a.left = c;
		a.right = d;
		b.left = e;
		b.right = f;
		
		System.out.println(isSumNode(root));
		
	}
}
