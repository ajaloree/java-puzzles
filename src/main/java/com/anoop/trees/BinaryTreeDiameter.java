package com.anoop.trees;

import java.util.concurrent.atomic.AtomicInteger;

import com.anoop.trees.ds.StringNode;

/**
 * Given a binary tree, write an efficient algorithm to compute the diameter of it. 
 * A binary tree diameter equals the total number of nodes on the longest path between 
 * any two leaves in it.
 * 
 * A simple solution would be to calculate the left and right subtree’s height for each 
 * node in the tree. The maximum node path that passes through a node will have a value 
 * one more than the sum of the height of its left and right subtree. Finally, the diameter 
 * is maximum among all maximum node paths for every node in the tree. The time complexity 
 * of this solution is O(n2) as there are n nodes in the tree, and for every node, we are 
 * calculating the height of its left and right subtree that takes O(n) time.
 * 
 * @author ajaloree
 *
 */
public class BinaryTreeDiameter {
	
	private static int depth(StringNode node) {
		
		if(node == null) {
			return 0;
		} else {
			return Math.max(depth(node.left), depth(node.right)) + 1;
		}
	}

	private static void calculateMaxNodePath(StringNode node, AtomicInteger accumulator) {
		
		if(node == null) {
			return;
		}
		
		int leftDepth = depth(node.left);
		int rightDepth = depth(node.right);
		int maxNodePath = leftDepth+rightDepth+1;
		
		if(accumulator.intValue() < maxNodePath) {
			accumulator.set(maxNodePath);
		}
		
		calculateMaxNodePath(node.left, accumulator);
		calculateMaxNodePath(node.right, accumulator);
	}
	
	public static int calculateDiameter(StringNode node) {
		AtomicInteger accumulator = new AtomicInteger(0);
		calculateMaxNodePath(node,accumulator);
		return accumulator.intValue();
	}
	
	public static void main(String[] args) {
		
		StringNode a = new StringNode("A");
		StringNode b = new StringNode("B");
		StringNode c = new StringNode("C");
		StringNode d = new StringNode("D");
		StringNode e = new StringNode("E");
		StringNode f = new StringNode("F");
		StringNode g = new StringNode("G");
		StringNode h = new StringNode("H");
		StringNode i = new StringNode("I");

		a.left = b;
		a.right = c;
		b.left = d;
		b.right = e;
		c.left = f;
		c.right = g;
		g.left = h;
		g.right = i;
		
		System.out.println(calculateDiameter(a));
		
	}
	
}
