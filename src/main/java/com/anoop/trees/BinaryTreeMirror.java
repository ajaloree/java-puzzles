package com.anoop.trees;

import com.anoop.trees.ds.IntNode;

/**
 * Given a binary tree, write an efficient algorithm to convert the binary tree into its mirror.
 * 
 * For example, the following binary trees are mirrors of each other:
 * 
 * 						1			| 			 1
 * 					   / \          |           / \
 *                    /   \         |          /   \
 *                   2     3        |         3     2
 *                  / \   / \       |        / \   / \
 *                 /   \ /   \      |       /   \ /   \
 *                4    5 6    7     |      7    6 5    4
 * 
 * @author ajaloree
 *
 */
public class BinaryTreeMirror {

	public static void mirror(IntNode node) {
		
		if(node == null) {
			return;
		}
		IntNode temp = node.left;
		node.left = node.right;
		node.right = temp;
		
		mirror(node.left);
		mirror(node.right);
	}
	
}
