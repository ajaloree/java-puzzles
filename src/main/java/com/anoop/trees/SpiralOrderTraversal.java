package com.anoop.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.anoop.trees.ds.IntNode;

/**
 * Spiral order traversal of a binary tree. 
 * Given a binary tree, print its nodes level by level in spiral order, 
 * i.e., all nodes present at level 1 should be printed first from left 
 * to right, followed by nodes of level 2 from right to left, followed 
 * by nodes of level 3 from left to right and so on. In other words, odd 
 * levels should be printed from left to right, and even levels should be 
 * printed from right to left or vice versa.
 * 
 * For example, the spiral level order traversal for the following tree is
 * 		
 * 								15
 * 							   / \
 *                            /   \
 *                           /     \
 *                          13     14
 *                         / \     / \
 *                        /   \   /   \
 *                       9     10 11   12
 *                      / \   / \ /\   /\
 *                     1   2 3  4 5 6 7  8 
 * 						
 * (15, 13, 14, 12, 11, 10, 9, 1, 2, 3, 4, 5, 6, 7, 8) 
 * 						   or 
 * (15, 14, 13, 9, 10, 11, 12, 8, 7, 6, 5, 4, 3, 2, 1)
 * 
 * @author ajaloree
 *
 */
public class SpiralOrderTraversal {

	public static String spiralPrintNodes(IntNode root) {
		
		StringBuilder sb = new StringBuilder();
		List<List<IntNode>>  nodesByLevel = getNodesByLevels(root);
		
		int i = 2;
		for(List<IntNode> nodes : nodesByLevel) {
			if(i%2 == 0) {
				for(int j = 0; j < nodes.size(); j++) {
					sb.append(nodes.get(j).data).append(" ");
				}
			} else {
				for(int j = nodes.size()-1; j >= 0; j--) {
					sb.append(nodes.get(j).data).append(" ");
				}
			}
			i++;
		}
		
		return sb.toString().trim();
	}
	
	private static List<List<IntNode>> getNodesByLevels(IntNode node) {
		
		List<List<IntNode>> nodesByLevel = new ArrayList<>();
		
		Queue<IntNode> queue = new LinkedList<>();
		queue.add(node);
		
		while(!queue.isEmpty()) {
			
			List<IntNode> nodesAtThisLevel = drainQueue(queue);
			nodesByLevel.add(nodesAtThisLevel);
			for(IntNode n : nodesAtThisLevel) {
				if(n.left != null) queue.add(n.left);
				if(n.right != null) queue.add(n.right);
			}
		}
		
		return nodesByLevel;
	}
	
	private static List<IntNode> drainQueue(Queue<IntNode> queue) {
		List<IntNode> nodes = new ArrayList<>();
		while(!queue.isEmpty()) {
			nodes.add(queue.remove());
		}
		return nodes;
	}
}
