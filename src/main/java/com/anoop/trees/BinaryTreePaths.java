package com.anoop.trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import com.anoop.trees.ds.StringNode;

public class BinaryTreePaths {
	
	private final StringNode root;
	private final Map<String, String> parentMap;
	private final Set<String> leaves;
	private final List<String> routes;
	
	public BinaryTreePaths(StringNode root) {
		this.root = root;
		parentMap = new HashMap<>();
		leaves = new HashSet<>();
		routes = new ArrayList<>();
		scanTree();
		scanRoutes();
	}
	
	private void scanTree() {
		
		if(root!=null) {
			Queue<StringNode> queue = new LinkedList<>();
			queue.add(root);
			parentMap.put(root.data, null);
			while(!queue.isEmpty()) {
				StringNode node = queue.poll();
				if(node.left != null) {
					parentMap.put(node.left.data, node.data);
					queue.add(node.left);
				}
				if(node.right != null) {
					parentMap.put(node.right.data, node.data);
					queue.add(node.right);
				}
				if(node.left == null && node.right == null) {
					leaves.add(node.data);
				}
			}
		}
	}
	
	private void scanRoutes() {
		
		for(String leaf : leaves) {
			StringBuilder bld = new StringBuilder();
			String node = leaf;
			while(node != null) {
				bld.append(node);
				node = parentMap.get(node);
			}
			routes.add(bld.toString());
		}
	}

	private String reverse(String str) {
		StringBuilder sb = new StringBuilder();
		for(int i = str.length()-1; i >= 0; i--) {
			sb.append(str.charAt(i));
		}
		return sb.toString();
	}

	public List<String> getPaths() {
		
		List<String> paths = new ArrayList<>();
		for(String route : routes) {
			String path = Arrays.stream(reverse(route).split("")).collect(Collectors.joining("->"));
			paths.add(path);
		}
		return paths;
	}
	
}