package com.anoop.citi;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class CitibankCodilityTask2 {
	
    public String solution(String S) {
    	
    	//Map to store city wise time ordered file extensions
    	Map<String, Map<String, String>> store = new TreeMap<>();//TreeMap not required here - can be a HashMap
    	//Map to store counts of images per city
    	Map<String, Integer> imageCountsByCity = new HashMap<>();
    	//Map to store final output for each city and timestamp pair
    	Map<String, String> tempStore = new HashMap<>();
    	
    	//Spting the string and populate the 'store' map
    	String[] splits = S.split("\\r?\\n");
    	for(String split : splits) {
    		String[] data = split.split(",");
    		String extension = data[0].split("\\.")[1].trim();
    		String city = data[1].trim();
    		String time = data[2].trim();
    		
    		Map<String, String> imagesByTimeMap = null;
    		if(store.containsKey(city)) {
    			imagesByTimeMap = store.get(city);
    		} else {
    			//Using a TreeMap to order data by time-stamp
    			imagesByTimeMap = new TreeMap<>();
    			store.put(city, imagesByTimeMap);
    		}
    		imagesByTimeMap.put(time, extension);
    	}
    	
    	//Count number of images for each city as size needs to known before to calculate amount of padding required
    	for(String key : store.keySet()) {
    		imageCountsByCity.put(key, store.get(key).size());
    	}
    	
    	//Create final output for each file and store it in a map with city+timestamp as key
    	for(String city : store.keySet()) {
    		int i = 1;
    		for(String time : store.get(city).keySet()) {
    			
    			String key = city+time;
    			String value = city+padLeftZeros(i+"", Integer.toString(imageCountsByCity.get(city)).length())+"."+store.get(city).get(time);
    			tempStore.put(key, value);
    			i++;
    		}
    	}
    	
    	//Query each row from original string and copy its final output in a strin builder. This is required as order needs to be preserved.
    	StringBuilder sbr = new StringBuilder();
    	for(String split : splits) {
    		String[] data = split.split(",");
    		String city = data[1].trim();
    		String time = data[2].trim();
    		sbr.append(tempStore.get(city+time)).append("\n");
    	}
     	
    	return sbr.toString();
    }

    private String padLeftZeros(String inputString, int length) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }
}
