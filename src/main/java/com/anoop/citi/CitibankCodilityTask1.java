package com.anoop.citi;

public class CitibankCodilityTask1 {
	
	public int solution(String S) {
		
		if(S == null || S.trim().length() == 0) return 0;
		
		String[] sentences = S.split("\\!+|\\.+|\\?+");
		int maxWords = 0;
		for(String sentence : sentences) {
			String[] words = sentence.trim().split("\\s+");
			if(words.length > maxWords) {
				maxWords = words.length;
			}
		}
		return maxWords;
	}
	
}
