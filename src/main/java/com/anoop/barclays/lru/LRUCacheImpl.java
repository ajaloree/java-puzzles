package com.anoop.barclays.lru;
/**
 * 
 * Requirements:
 * - Cache bound by limit parameter
 * - Last element evicted if limit exceeded
 * - Preserve access of keys
 * 
 * @author ajaloree
 *
 * @param <K>
 * @param <V>
 */

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LRUCacheImpl<K, V> implements LRUCache<K, V> {
	
	//Data structure to hold key value pairs
	private final Map<K, V> map;
	
	/**
	 * Data structure to hold the order of key access.
	 * This data structure should only hold n elements, where n = capacity.
	 * Also when an element is added in it, the oldest excess element should be deleted.
	 * So add in front and remove from end - possibly a Deque?
	 */
	private final Deque<K> deque;
	
	private final int capacity;
	
	public LRUCacheImpl(int capacity) {
		this.capacity = capacity;
		this.map = new HashMap<>(capacity);
		this.deque = new ArrayDeque<>(capacity);
	}
	
	/**
	 * If a new key is added:
	 * - Add element in the map
	 * - Delete key from back of queue, if deque size >= capacity
	 * - Add key in front of deque
	 */
	@Override
	public void put(K key, V value) {
		
		if(deque.size() < capacity) {
			deque.addFirst(key);
		} else {
			K keyToRemove = deque.removeLast();
			deque.addFirst(key);
			map.remove(keyToRemove);
		}
		map.put(key, value);
	}

	@Override
	public V get(K key) {
		
		if(deque.size() < capacity) {
			deque.addFirst(key);
		} else if(map.containsKey(key)) {
			K keyToRemove = deque.removeLast();
			deque.addFirst(key);
			if(keyToRemove != key) {
				map.remove(keyToRemove);
			}
		}
		return map.get(key);
	}

	@Override
	public String toString() {
		
		List<String> items = new ArrayList<>();
		for(K key : deque) {
			String val = "<";
			val += (key + "," + map.get(key));
			val += ">";
			items.add(val);
		}
		return items.stream().collect(Collectors.joining(","));
	}

	@Override
	public int size() {
		return map.size();
	}
}
