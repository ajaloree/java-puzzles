package com.anoop.barclays.foo_bar;

public class FooBarMain {

	public static void main(String[] args) {
		
		FooBar fb = new FooBar(10);
		Thread t1 = new Thread(() -> fb.foo());
		Thread t2 = new Thread(() -> fb.bar());
		t1.start();
		t2.start();
		
	}
}
